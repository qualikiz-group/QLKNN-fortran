classdef test_mexed < matlab.unittest.TestCase
    % Testcases based on RAPTOR
    % Opposed to RAPTOR, we compile ourselves on the CLI, instead of in
    % MATLAB functions
    
    % To dump input for pure-Fortran ingestion, use the write_namelist
    % function in the tools folder
  properties
    oldPath
    basePath
    raptorRoot
    qlknnRelPath
    qlknnNmlPath
    qlknnTubsRelPath
    qlknnBinPath
    input
    opts
    norms
    verbosity
    out_old
    dout_din_old
    useNNType
  end
  
  properties (ClassSetupParameter)
      qlknnNmlRelPath = struct('hyper', 'data/qlknn-hyper-namelists');
      NNType = struct('hyper', 0);
  end
  
  methods(TestClassSetup, ParameterCombination = 'sequential')
    function setupNewPath(testCase)
      p = path;
      testCase.addTeardown(@path,p);
      testCase.basePath = fullfile(fileparts(mfilename('fullfile')),'..');
    end

    function setupQlknnPaths(testCase, qlknnNmlRelPath)
      p = path;
      testCase.addTeardown(@path,p);
      testCase.qlknnRelPath = '..';
      testCase.qlknnTubsRelPath = 'tubs';
      testCase.qlknnNmlPath = fullfile(testCase.raptorRoot, testCase.qlknnRelPath, qlknnNmlRelPath);
      testCase.qlknnBinPath = fullfile(testCase.raptorRoot, testCase.qlknnRelPath, 'bin');
    end

    function setupQlknnMexPath(testCase)
      if ~is_in_path(testCase.qlknnBinPath)
        addpath(testCase.qlknnBinPath)
        disp(['Added to path:', testCase.qlknnBinPath])
      else
        disp(['Not added to path... :', testCase.qlknnBinPath])
      end
    end
    
    function setupInput(testCase)
      %#ok<*PROP>
      n_rho = 24;
      input = zeros(n_rho, 11);
      input(:,1) = 1; %Zeff
      input(:,2) = linspace(0, 13, n_rho); %Ati
      input(:,3) = 9; %Ate
      input(:,4) = 3; %An
      input(:,5) = 2; %q
      input(:,6) = 1; %smag
      input(:,7) = 0.449951; %x
      input(:,8) = 1; % Ti_Te
      input(:,9) = -2.000217201545864; %log(Nustar)
      input(:,10) = linspace(-0.4, 0.1, n_rho); %gammaE
      input(:,11) = 7.938652843198319; %Te (victor rule)
      testCase.input = input;
    end

    function setupOpts(testCase, NNType)
      % Default options structure to detect changes to it
      testCase.verbosity = 0;
      opts.use_ion_diffusivity_networks = false;
      opts.apply_victor_rule = false;
      opts.use_effective_diffusivity = true;
      opts.calc_heat_transport = true;
      opts.calc_part_transport = true;
      opts.use_ETG = true;
      opts.use_ITG = true;
      opts.use_TEM = true;
      opts.apply_stability_clipping = false; % currently dont care
      opts.merge_modes = false;
      opts.force_evaluate_all = true;

      opts.constrain_inputs = false;
      opts.constrain_outputs = false;

      opts.min_input = [1., 0., 0., -5., 0.66, -1., .09, 0.25, -5., -100., -100.];
      opts.max_input = [3., 14., 14., 6., 15., 5., .99, 2.5, 0., 100., 100.];
      opts.margin_input = 0.95;
      if NNType == 0
          if opts.merge_modes
            n_out = 10;
          else
            n_out = 20;
          end
      elseif NNType == 1
         if opts.merge_modes
            n_out = 4;
          else
            n_out = 7;
          end  
      end
      opts.min_output = repmat(-100, 1, n_out);
      opts.max_output = repmat(100, 1, n_out);
      opts.margin_output = repmat(1, 1, n_out);
      opts.shear_minus_alpha = false; % Use "jonathan rule" for alpha
      testCase.opts = opts;
      testCase.useNNType = NNType;
    end
    
    function setupNorms(testCase)
      norms.A1 = repmat(2, [1, size(testCase.input, 1)]);
      norms.a = 1;
      norms.R0 = 3;
      testCase.norms = norms;
    end
  end %method TestClassSetup

  methods (Test)
    function testSimpleRun(testCase)
     opts = testCase.opts;
     qlknn_mex(testCase.qlknnNmlPath, testCase.input, testCase.verbosity, opts);
    end
    
        function testJacobianRun(testCase)
     opts = testCase.opts;
     func = @(input)  qlknn_mex(testCase.qlknnNmlPath, input, testCase.verbosity, opts, struct(), testCase.useNNType);
     [out, dout_din] = func(testCase.input);
    end

    function testJacobianSimplest(testCase)
     opts = testCase.opts;
     func = @(input)  qlknn_mex(testCase.qlknnNmlPath, input, testCase.verbosity, opts, struct(), testCase.useNNType);
     passed = check_jacobian(testCase.input, func, testCase.verbosity);
     testCase.assertTrue(passed);
    end

    function testJacobianInputClip(testCase)
      opts = testCase.opts;
      opts.constrain_inputs = true;
      opts.max_input(:, 1:3) = 0.5 * max(testCase.input(:, 1:3));
      func = @(input)  qlknn_mex(testCase.qlknnNmlPath, input, testCase.verbosity, opts, struct(), testCase.useNNType);
      passed = check_jacobian(testCase.input, func, testCase.verbosity);
      testCase.assertTrue(passed);
    end

    function testJacobianOutputClip(testCase)
      opts = testCase.opts;
      opts.constrain_outputs = true;
      opts.max_output(:, 1:3) = 1e-6;
      func = @(input)  qlknn_mex(testCase.qlknnNmlPath, input, testCase.verbosity, opts, struct(), testCase.useNNType);
      passed = check_jacobian(testCase.input, func, testCase.verbosity);
      testCase.assertTrue(passed);
    end

    function testJacobiaApplyStabilityClip(testCase)
      % This test currently always passes, as
      % impose_leading_flux_constraints is done before applying stability
      % clipping
      opts = testCase.opts;
      opts.apply_stability_clipping = true;
      func = @(input)  qlknn_mex(testCase.qlknnNmlPath, input, testCase.verbosity, opts, struct(), testCase.useNNType);
      passed = check_jacobian(testCase.input, func, testCase.verbosity);
      testCase.assertTrue(passed);
    end

    function testJacobianVictor(testCase)
      opts = testCase.opts;
      opts.apply_victor_rule = true;
      assumeEqual(testCase, testCase.useNNType, 0)
      func = @(input)  qlknn_mex(testCase.qlknnNmlPath, input, testCase.verbosity, opts, testCase.norms, testCase.useNNType);
      passed = check_jacobian(testCase.input, func, testCase.verbosity);
      testCase.assertTrue(passed);
    end

    function testJacobianMergeModes(testCase)
      opts = testCase.opts;
      opts.merge_modes = true;
      func = @(input)  qlknn_mex(testCase.qlknnNmlPath, input, testCase.verbosity, opts, struct(), testCase.useNNType);
      passed = check_jacobian(testCase.input, func, testCase.verbosity);
      testCase.assertTrue(passed);
    end
  end %method Test
end

function is_in_path = is_in_path(Folder)
  pathCell = regexp(path, pathsep, 'split');
  if ispc  % Windows is not case-sensitive
    is_in_path = any(strcmpi(Folder, pathCell));
  else
    is_in_path = any(strcmp(Folder, pathCell));
  end
  return
end
