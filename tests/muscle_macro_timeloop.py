import logging
import os

import numpy as np

from libmuscle import Grid, Instance, Message
from ymmsl import Operator

inp = np.array(
    [
        [
            1.0,
            2.0,
            9.0,
            3.0,
            2.0,
            1.0,
            0.449951,
            1.0,
            -2.000217201545864,
            0.0,
            7.938652843198319,
        ],
        [
            1.0,
            2.4782608695652173,
            9.0,
            3.0,
            2.0,
            1.0,
            0.449951,
            1.0,
            -2.000217201545864,
            -0.03043478260869565,
            7.938652843198319,
        ],
        [
            1.0,
            2.9565217391304346,
            9.0,
            3.0,
            2.0,
            1.0,
            0.449951,
            1.0,
            -2.000217201545864,
            -0.0608695652173913,
            7.938652843198319,
        ],
        [
            1.0,
            3.4347826086956523,
            9.0,
            3.0,
            2.0,
            1.0,
            0.449951,
            1.0,
            -2.000217201545864,
            -0.09130434782608696,
            7.938652843198319,
        ],
        [
            1.0,
            3.9130434782608696,
            9.0,
            3.0,
            2.0,
            1.0,
            0.449951,
            1.0,
            -2.000217201545864,
            -0.1217391304347826,
            7.938652843198319,
        ],
        [
            1.0,
            4.391304347826087,
            9.0,
            3.0,
            2.0,
            1.0,
            0.449951,
            1.0,
            -2.000217201545864,
            -0.15217391304347824,
            7.938652843198319,
        ],
        [
            1.0,
            4.869565217391305,
            9.0,
            3.0,
            2.0,
            1.0,
            0.449951,
            1.0,
            -2.000217201545864,
            -0.1826086956521739,
            7.938652843198319,
        ],
        [
            1.0,
            5.3478260869565215,
            9.0,
            3.0,
            2.0,
            1.0,
            0.449951,
            1.0,
            -2.000217201545864,
            -0.21304347826086956,
            7.938652843198319,
        ],
        [
            1.0,
            5.826086956521739,
            9.0,
            3.0,
            2.0,
            1.0,
            0.449951,
            1.0,
            -2.000217201545864,
            -0.2434782608695652,
            7.938652843198319,
        ],
        [
            1.0,
            6.304347826086957,
            9.0,
            3.0,
            2.0,
            1.0,
            0.449951,
            1.0,
            -2.000217201545864,
            -0.27391304347826084,
            7.938652843198319,
        ],
        [
            1.0,
            6.782608695652174,
            9.0,
            3.0,
            2.0,
            1.0,
            0.449951,
            1.0,
            -2.000217201545864,
            -0.3043478260869565,
            7.938652843198319,
        ],
        [
            1.0,
            7.260869565217392,
            9.0,
            3.0,
            2.0,
            1.0,
            0.449951,
            1.0,
            -2.000217201545864,
            -0.33478260869565213,
            7.938652843198319,
        ],
        [
            1.0,
            7.739130434782609,
            9.0,
            3.0,
            2.0,
            1.0,
            0.449951,
            1.0,
            -2.000217201545864,
            -0.3652173913043478,
            7.938652843198319,
        ],
        [
            1.0,
            8.217391304347826,
            9.0,
            3.0,
            2.0,
            1.0,
            0.449951,
            1.0,
            -2.000217201545864,
            -0.39565217391304347,
            7.938652843198319,
        ],
        [
            1.0,
            8.695652173913043,
            9.0,
            3.0,
            2.0,
            1.0,
            0.449951,
            1.0,
            -2.000217201545864,
            -0.4260869565217391,
            7.938652843198319,
        ],
        [
            1.0,
            9.173913043478262,
            9.0,
            3.0,
            2.0,
            1.0,
            0.449951,
            1.0,
            -2.000217201545864,
            -0.45652173913043476,
            7.938652843198319,
        ],
        [
            1.0,
            9.652173913043478,
            9.0,
            3.0,
            2.0,
            1.0,
            0.449951,
            1.0,
            -2.000217201545864,
            -0.4869565217391304,
            7.938652843198319,
        ],
        [
            1.0,
            10.130434782608695,
            9.0,
            3.0,
            2.0,
            1.0,
            0.449951,
            1.0,
            -2.000217201545864,
            -0.5173913043478261,
            7.938652843198319,
        ],
        [
            1.0,
            10.608695652173914,
            9.0,
            3.0,
            2.0,
            1.0,
            0.449951,
            1.0,
            -2.000217201545864,
            -0.5478260869565217,
            7.938652843198319,
        ],
        [
            1.0,
            11.08695652173913,
            9.0,
            3.0,
            2.0,
            1.0,
            0.449951,
            1.0,
            -2.000217201545864,
            -0.5782608695652174,
            7.938652843198319,
        ],
        [
            1.0,
            11.565217391304348,
            9.0,
            3.0,
            2.0,
            1.0,
            0.449951,
            1.0,
            -2.000217201545864,
            -0.608695652173913,
            7.938652843198319,
        ],
        [
            1.0,
            12.043478260869566,
            9.0,
            3.0,
            2.0,
            1.0,
            0.449951,
            1.0,
            -2.000217201545864,
            -0.6391304347826087,
            7.938652843198319,
        ],
        [
            1.0,
            12.521739130434783,
            9.0,
            3.0,
            2.0,
            1.0,
            0.449951,
            1.0,
            -2.000217201545864,
            -0.6695652173913043,
            7.938652843198319,
        ],
        [
            1.0,
            13.0,
            9.0,
            3.0,
            2.0,
            1.0,
            0.449951,
            1.0,
            -2.000217201545864,
            -0.7,
            7.938652843198319,
        ],
    ]
)
qlknn_hyper_names = [
    "net_efeetg_gb"
    "net_efeitg_gb_div_efiitg_gb"
    "net_efetem_gb"
    "net_efiitg_gb"
    "net_efitem_gb_div_efetem_gb"
    "net_pfeitg_gb_div_efiitg_gb"
    "net_pfetem_gb_div_efetem_gb"
    "net_dfeitg_gb_div_efiitg_gb"
    "net_dfetem_gb_div_efetem_gb"
    "net_vteitg_gb_div_efiitg_gb"
    "net_vtetem_gb_div_efetem_gb"
    "net_vceitg_gb_div_efiitg_gb"
    "net_vcetem_gb_div_efetem_gb"
    "net_dfiitg_gb_div_efiitg_gb"
    "net_dfitem_gb_div_efetem_gb"
    "net_vtiitg_gb_div_efiitg_gb"
    "net_vtitem_gb_div_efetem_gb"
    "net_vciitg_gb_div_efiitg_gb"
    "net_vcitem_gb_div_efetem_gb"
    "net_gam_leq_gb"
]
qlknn_hyper_input_names = [
    r"Z_{eff}",
    r"RLT_i",
    r"RLT_e",
    r"RLn",
    r"q",
    r"\hat{s}",
    r"x",
    r"T_i/T_e",
    r"\log{\nu^*}",
    r"\gamma_{ExB}",
    r"T_e",
]
qlknn_hyper_output_names = [
    r"q_{e,ETG}",
    r"q_{e,ITG}",
    r"q_{e,TEM}",
    r"q_{i,ITG}",
    r"q_{i,TEM}",
    r"\Gamma_{e,ITG}",
    r"\Gamma_{e,TEM}",
    r"D_{e,ITG}",
    r"D_{e,TEM}",
    r"V_{t,e,ITG}",
    r"V_{t,e,TEM}",
    r"V_{c,e,ITG}",
    r"V_{c,e,TEM}",
    r"D_{i,ITG}Di",
    r"D_{i,TEM}Di",
    r"V_{t,i,ITG}",
    r"V_{t,i,TEM}",
    r"V_{c,i,ITG}",
    r"V_{c,i,TEM}",
    r"\gamma_0",
]
qlknn_hyper_merged_output_names = [
    r"q_{e}",
    r"q_{e,ETG}",
    r"q_{i}",
    r"\Gamma_{e}",
    r"D_{e}",
    r"V_{t,e}",
    r"V_{c,e}",
    r"D_{i}",
    r"V_{t,i}",
    r"V_{c,i}",
]


def diffusion() -> None:
    """A simple diffusion model on a 1d grid.

    The state of this model is a 1D grid of concentrations. It sends
    out the state on each timestep on `state_out`, and can receive an
    updated state on `state_in` at each state update.
    """
    logger = logging.getLogger()
    instance = Instance(
        {
            Operator.O_I: ["state_out"],
            Operator.S: ["state_in"],
            Operator.O_F: ["final_state_out"],
        }
    )

    while instance.reuse_instance():
        # F_INIT
        t_max = instance.get_setting("t_max", "float")
        dt = instance.get_setting("dt", "float")
        x_max = instance.get_setting("x_max", "float")
        dx = instance.get_setting("dx", "float")
        d = instance.get_setting("d", "float")

        U = np.zeros(int(round(x_max / dx))) + 1e-20
        U[25] = 2.0
        U[50] = 2.0
        U[75] = 2.0
        Us = U

        t_cur = 0.0
        # The timeloop that calls our Fortran code
        while t_cur + dt <= t_max:
            # O_I
            t_next = t_cur + dt
            if t_next + dt > t_max:
                t_next = None
            outp = np.ndarray((inp.shape[0], 10))
            cur_state_msg = Message(t_cur, t_next, Grid(inp, ["rho", "n_in"]))
            # We're sending our state state_out to the input port as defined in
            # ymmsl. This is received in the fortran code
            instance.send("state_out", cur_state_msg)

            # Now our code can do work. When its done, it should send on its
            # port final_state, which we will try to receive below
            # S
            msg = instance.receive("state_in")
            # msg = instance.receive('state_in', default=cur_state_msg)
            if msg.timestamp > t_cur + dt:
                logger.warning("Received a message from the future!")
            np.copyto(outp, msg.data.array)

            try:
                outps = np.vstack((outps, outp))
            except NameError:
                outps = outp
            logger.info(f"Received result for t_cur={t_cur}")

            # dU = np.zeros_like(U)
            # dU[1:-1] = d * laplacian(U, dx) * dt
            # dU[0] = dU[1]
            # dU[-1] = dU[-2]

            # U += dU
            # Us = np.vstack((Us, U))
            t_cur += dt

        # Our while loop is done and "converged"
        # O_F
        final_state_msg = Message(t_cur, data=Grid(U, ["x"]))
        instance.send("final_state_out", final_state_msg)

        if "DONTPLOT" not in os.environ and "SLURM_NODENAME" not in os.environ:
            from matplotlib import pyplot as plt
            from matplotlib import gridspec

            fig = plt.figure()
            gs = gridspec.GridSpec(
                22, 20, hspace=20, left=0.05, right=0.95, bottom=0.05, top=0.95
            )
            lbound = 20
            rbound = 2
            from math import floor

            axes = {
                "heat": plt.subplot(gs[: floor(lbound / 4), rbound:]),
                "diffusivity": plt.subplot(
                    gs[floor(lbound / 4) : floor(lbound / 2), rbound:]
                ),
                "pinch": plt.subplot(
                    gs[floor(lbound / 2) : floor(3 * lbound / 4), rbound:]
                ),
                "particles": plt.subplot(gs[floor(3 * lbound / 4) : lbound, rbound:]),
                # "flux": plt.subplot(gs[:lbound, :2]),
                # "freq_low": plt.subplot(gs[:10, 2]),
                # "freq_high": plt.subplot(gs[:10, 3]),
                # "grow_low": plt.subplot(gs[10:-2, 2]),
                # "grow_high": plt.subplot(gs[10:-2, 3]),
                # "dimx_slider": plt.subplot(gs[-2:, 2:]),
            }
            # ax = axes["myslice"] = plt.subplot(gs[lbound:, :2])
            for name, outp in zip(qlknn_hyper_merged_output_names, outps.T):
                mathname = f"${name}$"
                if "q" in name:
                    axes["heat"].plot(outp, label=mathname)
                elif "D" in mathname:
                    axes["diffusivity"].plot(outp, label=mathname)
                elif "V" in mathname:
                    axes["pinch"].plot(outp, label=mathname)
                elif r"\Gamma" in mathname:
                    axes["particles"].plot(outp, label=mathname)
                else:
                    raise Exception(f"Unkown variable name {name}")
            axes["heat"].set_ylabel(r"q")
            axes["diffusivity"].set_ylabel(r"V")
            axes["pinch"].set_ylabel(r"D")
            axes["particles"].set_ylabel(r"$\Gamma$")
            axes["particles"].set_xlabel(f"time $\delta t_{{macro}} = {dt}s$")
            for name, ax in axes.items():
                ax.legend()
            plt.tight_layout()
            fig.savefig("result.png")


if __name__ == "__main__":
    logging.basicConfig()
    logging.getLogger().setLevel(logging.INFO)
    diffusion()
