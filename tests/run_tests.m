function [passed,results] = run_tests(test_case)
if nargin==0
  test_case = 'basic';
end

import matlab.unittest.TestSuite;
import matlab.unittest.selectors.HasName;
import matlab.unittest.constraints.EndsWithSubstring;
import matlab.unittest.constraints.ContainsSubstring;
import matlab.unittest.selectors.HasParameter

switch test_case
  case 'basic'
    test_file = 'test_mexed.m';
    % basic matlab tests
    suite_basic = TestSuite.fromFile(test_file);
    suite = [suite_basic];
  otherwise
    error('undefined test case');
end

%% run it
fprintf('Start test case: %s\n%s\n\n',test_case,datestr(now));
results = run(suite);
disp(table(results));
fprintf('\nTotal test duration: %5.2fs\n',sum(table(results).Duration))

if all([results.Passed])
  fprintf('\nPassed all tests\n')
  passed = true;
else
  fprintf('\nSome tests Failed or Incomplete\n')
  if any([results.Incomplete])
    fprintf('\nIncomplete:\n')
    disp(table(results([results.Incomplete])))
    passed = true;
  end
  if any([results.Failed])
    fprintf('\nFailed:\n')
    disp(table(results([results.Failed])));
    passed = false;
  end
end

end
