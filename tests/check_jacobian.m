function [passed] = check_jacobian(orig_in, func,varargin)

doplot=false;
if numel(varargin)>=1
  doplot = varargin{1};
end

tol = 1e-4; % accept maximum this number for all residual norms
pertsize = 1e-6; % relative size of perturbation

% random perturbation
din = pertsize/norm(orig_in)*rand(size(orig_in));

[out1,dout_din] = func(orig_in);
[out2,~] = func(orig_in+din);
dout = out2-out1; % function difference due to perturbation

nRho = size(orig_in, 1);
nInp = size(orig_in, 2);
nOutp = size(out1, 2);

if ~all(size(dout_din) == [nRho nOutp nInp])
  error('Shape of jacobian given by func is incorrect, should be (%i, %i, %i)', nRho, nOutp, nInp)
end

err = zeros(nRho,1);
all_rho_passed = true(nRho,1);
for irho = 1:nRho
  dprofa = shiftdim(dout_din(irho,:,:),1)*din(irho,:)';
  err(irho) = calc_error(dprofa,dout(irho,:)',out1(irho,:)');
  rho_passed = all(abs(err)<tol);
  all_rho_passed(irho) = rho_passed;
  if doplot && ~rho_passed
    subplot(211)
    plot([dprofa,dout(irho,:)'], '-o')
    subplot(212);
    plot(dprofa-dout(irho,:)', '-o');
    if numel(dprofa-dout(irho,:)') == 1
        hold on
        plot((1 + eps) * (dprofa-dout(irho,:))', '-o');
    end
    keyboard
  end
end

passed = all(all_rho_passed);

return

function err = calc_error(dprofa,dprofn,prof0)
% generic function to calculate calculate relative numerical vs analytical error in Jacobian
% err = ||numerical-analytical||/||analytical||

if ~isnan(dprofa)
    if norm(dprofn) == 0 && norm(dprofa) == 0
        err = 0; % zero error because no dependence
    elseif (norm(dprofn)/norm(prof0) < 10*eps) && ...
            (norm(dprofa)/norm(prof0) < 10*eps)
        err = 0; % zero because relative change is too small (but nonzero)
    elseif norm(dprofn) == 0
        err = inf; % they should not be zero while the other is nonzero
    elseif norm(dprofa) == 0
        err = -inf; % they should not be zero while the other is nonzero
    else
        err = norm(dprofn-dprofa)./(norm(dprofa)+eps); % relative error
    end
else % if Jacobian is does not exist
    if norm(dprofn) == 0
        % no dependence (numerically) and Jacobian undefined
        err = 0;
    else
        err = NaN; % numerical dependence, but no Jacobian defined!
    end
    
end
return
