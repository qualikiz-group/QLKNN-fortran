function run_matlab_tests(test_case)
try
   cd tests
   passed = run_tests(test_case);
   exit_code = int32(~passed); % convert to bash shell convention
catch ME
   disp(getReport(ME))
   exit_code = 1;
end
exit(exit_code);
