# This file is part of QLKNN-fortran
# You should have received the QLKNN-fortran LICENSE in the root of the project
$(call SUBPROJECT_depend, QLKNN CLAF90)

$(call SUBPROJECT_reset_local)
$(call SUBPROJECT_set_local_dir_here)

# From level 1 training:
#ifeq ($(F90), ifort)
## INTEL - LINKS TO THE IMAS LIBRARY AND INCLUDE DIRECTORY
#  F90FLAGS=-fPIC -fpp -g # FPIC AND PREPROCESSING OPTIONS
#  INCLUDE=-I. `pkg-config imas-ifort --cflags`
#  LIBS=`pkg-config imas-ifort --libs`
#else
## GFORTRAN - LINKS TO THE IMAS LIBRARY AND INCLUDE DIRECTORY
#  F90=gfortran # FORTRAN COMPILER
#  F90FLAGS=-fPIC -cpp # FPIC AND PREPROCESSING OPTIONS
#  INCLUDE=-I. `pkg-config imas-gfortran --cflags`
#  LIBS=`pkg-config imas-gfortran --libs`
#endif

# with module load IMAS/3.30.0-4.8.5
# pkg-config imas-ifort --cflags
#   -I/work/imas/core/IMAS/3.30.0-4.8.5/include/ifort -I/work/imas/core/IMAS/3.30.0-4.8.5/include
# pkg-config imas-ifort --libs
#   -Wl,--defsym,AL_VER_4.8.5=0 -Wl,--defsym,DD_VER_3.30.0=0 -L/work/imas/core/IMAS/3.30.0-4.8.5/lib -limas-ifort-3.30.0 -limas
# pkg-config imas-gfortran --cflags
#   -I/work/imas/core/IMAS/3.30.0-4.8.5/include/gfortran -I/work/imas/core/IMAS/3.30.0-4.8.5/include
# pkg-config imas-gfortran --libs
#   -Wl,--defsym,AL_VER_4.8.5=0 -Wl,--defsym,DD_VER_3.30.0=0 -L/work/imas/core/IMAS/3.30.0-4.8.5/lib -limas-gfortran-3.30.0 -limas

# TODO: Link to IMAS a bit smarter
IMAS_DEFS=-Wl,--defsym,AL_VER_4.8.5=0 -Wl,--defsym,DD_VER_3.30.0=0
ifeq ($(TOOLCHAIN),gcc)
  $(info Detected TOOLCHAIN=$(TOOLCHAIN))
  QLKNN_EXTRA_FFLAGS+=-I../access-layer/fortraninterface/gfortran -I../access-layer/hdc_ual/src/
  QLKNN_EXTRA_LFLAGS+=$(IMAS_DEFS) -L../access-layer/fortraninterface -limas-gfortran-3.30.0 -limas
else ifeq ($(TOOLCHAIN),intel)
  $(info Detected TOOLCHAIN=$(TOOLCHAIN))
  QLKNN_EXTRA_FFLAGS+=-I../access-layer/fortraninterface/gfortran
  QLKNN_EXTRA_LFLAGS+=$(IMAS_DEFS) -L../access-layer/fortraninterface -limas-gfortran-3.30.0 -limas
else ifeq ($(TOOLCHAIN),pgi)
  $(info Detected TOOLCHAIN=$(TOOLCHAIN))
  $(error PGI unsupported for QLKNN-IMAS)
else
  $(error Unknown TOOLCHAIN=$(TOOLCHAIN))
endif

ifeq ($(TUBSCFG_MPI),1)
  $(info Detected MPI compilation, TUBSCFG_MPI=$(TUBSCFG_MPI))
else ifeq ($(TUBSCFG_MPI),0)
  $(info Serial compilation, TUBSCFG_MPI=$(TUBSCFG_MPI))
endif

# Check if we are compiling with MKL
ifeq ($(TUBSCFG_MKL),1)
  $(info Detected MKL compilation, TUBSCFG_MKL=$(TUBSCFG_MKL))
else ifeq ($(TUBSCFG_MKL),0)
  $(info Not compiling with MKL, TUBSCFG_MKL=$(TUBSCFG_MKL))
else
  $(info Unknown TUBSCFG_MKL $(TUBSCFG_MKL))
endif

$(eval include $(QLKNNROOT_)/flags.make)

$(call LOCAL_add,\
  src/qlknn_imas_driver.f90 \
  src/qlknn_imas_standalone.f90 \
)

$(call LOCAL_mod_dep, src/qlknn_imas_standalone.f90, kinds.mod qlknn_imas_driver.mod )
