# This file is part of QLKNN-fortran
# You should have received the QLKNN-fortran LICENSE in the root of the project
from collections import OrderedDict

import pandas as pd
import numpy as np
import f90nml
from IPython import embed

from qualikiz_tools.misc.conversion import calc_te_from_nustar
from json_nn_to_namelist import qlknn_9D_feature_names, qlknn_jetexp_feature_names

def generate_input_namelist(feature_names, scann=24, target_path='test.nml'):
    inp = pd.DataFrame({'Ati': np.linspace(2, 13, scann)})
    inp['Ti_Te']  = 1
    inp['Zeff']  = 1
    inp['An']  = 3
    inp['Ate']  = 9
    inp['q'] = 2
    inp['smag'] = 1
    inp['Nustar'] = 0.009995
    inp['x'] = 0.449951
    inp['logNustar'] = np.log10(inp['Nustar'])
    inp['Zeff'] = 1
    inp['Machtor'] = np.array(np.linspace(0.4, 0.1, scann))
    inp['gammaE'] = np.array(np.linspace(0, -0.7, scann))
    inp['Te'] = calc_te_from_nustar(inp['Zeff'], 5, 10**inp['logNustar'], inp['q'], 3, 1, inp['x'])
    inp['Ane'] = inp['An']
    inp['alpha'] = 0
    inp['Ani0'] = inp['Ane']
    inp['Ani1'] = inp['Ani0']
    inp['Ati0'] = inp['Ati']
    inp['normni0'] = 1
    inp['normni1'] = 0
    inp['Ti_Te0'] = inp['Ti_Te']
    inp = inp.loc[:, feature_names]
    if inp.isna().any().any():
        raise Exception('Found NaNs in columns {!s}. Extend the inp template in {!s}'.format([name for name, val in inp.isna().any().items() if val], __file__))
    nml_fields = OrderedDict([
        ('sizes', {'n_in': inp.shape[1], 'n_rho': inp.shape[0]}),
        ('test', {'input': np.asfortranarray(inp.values).tolist()}),
    ])
    nml = f90nml.Namelist(nml_fields)
    nml.write(target_path, force=True)

if __name__ == '__main__':
    import argparse
    parser = argparse.ArgumentParser(description='Generate a standardish input namelist file for QLKNN testing')
    parser.add_argument('net_type', type=str, help='',
                        nargs='?',
                        choices=['hyper', 'fullflux', 'hornnet', 'jetexp', 'adept'], default='hyper')
    parser.add_argument('target_path', type=str, help='',
                        nargs='?',
                        default='tests/test.nml')
    parser.add_argument('--nrho', type=int, help='',
                        nargs='?',
                        default=24)
    args = parser.parse_args()
    if args.net_type in ['hyper', 'fullflux', 'hornnet']:
        feature_names = qlknn_9D_feature_names + ['gammaE', 'Te']
    else:
        feature_names = qlknn_jetexp_feature_names
    generate_input_namelist(feature_names, target_path=args.target_path, scann=args.nrho)
