# This file is part of QLKNN-fortran
# You should have received the QLKNN-fortran LICENSE in the root of the project
import subprocess as sub
import os
import argparse
import socket
import re

from IPython import embed
import pandas as pd

from namelist_tools import generate_input_namelist
from json_nn_to_namelist import qlknn_9D_feature_names, qlknn_jetexp_feature_names

env_commands = {
    'pgi': 'module purge; module load pgi/14.4 openmpi/1.8.2',
    'intel': 'module purge; module load ifort/12.0 openmpi-ifort-12.0/2.1.0',
    'ifort18': 'module purge; module load ifort/2018.2.046 openmpi/3.0.1',
    'gcc': 'module purge; module load gfortran/4.8 openmpi/1.8.2',
}

def process_run(env_cmd, num_cores, nrho, repeat, binary_path, num_trials, jac, verbosity=0):
    generate_input_namelist(qlknn_9D_feature_names + ['gammaE', 'Te'], target_path='prof.nml', scann=nrho)
    cmd = [
        env_cmd + ';',
        'mpirun',
        '-n', str(num_cores),
        binary_path,
        '-t', str(num_trials),
        '--merge-modes',
        '--use-effective-diffusivity',
        '--verbosity', str(verbosity),
        '--input=prof.nml',
    ]
    tests = []
    if jac == 'jac':
        cmd.append('--jacobian')
    cmd = ' '.join(cmd)
    for repeat_num in range(repeat):
        print('Running', cmd)
        outp = sub.check_output(cmd, shell=True)
        if len(outp) == 0:
            print('Binary run failed! Abort.')
            raise Exception
        string_outp = [line.decode('UTF-8') for line in outp.splitlines()]
        for line in string_outp:
            if line.startswith('Time'):
                runtime = float(line.split('=')[1].strip().split(' ')[0])
                test_result = (nrho, num_cores, repeat_num, runtime)
                tests.append(test_result)
    os.remove('prof.nml')
    return tests

def process_toolchain(toolchain, report_prefix='bench_result'):
    env_cmd = env_commands[toolchain]
    #print("Running '{!s}' to prepare env".format(env_cmd))
    #env_cmd_res = sub.run(env_cmd, shell=True)
    #if env_cmd_res.returncode != 0:
    #    print('Could not prepare toolchain {!s}. Skipping!'.format(toolchain))
    #    raise Exception
    #    continue
    for mkl in [0, 1]:
        if toolchain == 'intel' and mkl == 1:
            print('Skipping MKL intel for now')
            continue
        make_cmd = '{!s}; make BUILD=release TUBSCFG_MPI=1 TUBSCFG_MKL={:d} TOOLCHAIN={!s} VERBOSE=1'.format(env_cmd, mkl, toolchain)
        print("Running '{!s}' to build binary".format(make_cmd))
        make_cmd_res = sub.run(make_cmd, shell=True)
        if make_cmd_res.returncode != 0:
            print('Could not build {!s}. Skipping!'.format(toolchain))
            raise Exception
            continue
        binary_name = 'bin/qlknn_hyper-{!s}-release-default-mpi'.format(toolchain)
        if mkl == 1:
            binary_name += '-mkl'
        binary_name += '.exe'
        for jac in ['jac', 'nojac']:
            tests = []
            for nrho in nrhos:
                for num_cores in num_cores_trails:
                    this_test  = process_run(env_cmd, num_cores, nrho, repeat, binary_name, trails[jac], jac, verbosity=verbosity)
                    tests.extend(this_test)


            df = test_restult_to_pandas(tests)
            df.to_csv(report_prefix + '_{!s}_{!s}_MKL{:d}.csv'.format(toolchain, jac, mkl))

def test_restult_to_pandas(tests):
    df = pd.DataFrame(tests, columns=['nrho', 'num_cores', 'repeat_num', 'runtime'])
    df.set_index(['nrho', 'num_cores', 'repeat_num'], inplace=True)
    std = df.std(level=['nrho', 'num_cores'])
    std.columns = ['std_' + col for col in std.columns]
    mean = df.mean(level=['nrho', 'num_cores'])
    mean.columns = ['mean_' + col for col in mean.columns]
    summary = pd.concat([mean, std], axis=1)
    summary = summary.sort_index()
    print(summary)
    return df

if __name__ == '__main__':
    start_dir = os.path.abspath(os.getcwd())
    this_dir = os.path.dirname(os.path.abspath(__file__))
    os.chdir(os.path.join(this_dir, '..'))

    toolchains = ['pgi', 'intel', 'gcc']
    mkls = [0, 1]
    trails = {
        'jac': 1000,
        'nojac': 1000,
    }
    verbosity = 0

    parser = argparse.ArgumentParser(description='Tools for binary profiling.')
    parser.add_argument('case', type=str, help='Case to be profiled', nargs='?', default='full')
    parser.add_argument('--binaries', type=str, help='Binaries to profile. Only read when case=specific', nargs='?')
    parser.add_argument('--repeat', type=int, help='Amount of times to repeat measurement', default=5, nargs='?')
    args = parser.parse_args()
    repeat = args.repeat

    if args.case == 'full':
        if args.binaries:
            print('--binaries detected, but case is {!s}, ignoring..'.format(args.case))
        toolchains = ['pgi', 'intel', 'gcc']
        num_cores_trails = [1, 2, 4, 6, 7, 8, 9]
        nrhos = [24]
        prefix = 'bench_result'
    elif args.case == 'radial':
        if args.binaries:
            print('--binaries detected, but case is {!s}, ignoring..'.format(args.case))
        toolchains = ['pgi']
        num_cores_trails = [1, 2, 7]
        nrhos = [12, 24, 48, 96, 101]
        prefix = 'bench_radial'
    elif args.case == 'specific':
        if not args.binaries:
            raise Exception('Please profile comma-separated list of binaries to test')
        binary_list = args.binaries.split(',')
        num_cores = 1
        nrho = 24
        jac = 'jac'
        prefix = 'bench_bin'

    print('Starting profiling on {!s}'.format(socket.getfqdn()))
    git_cmd = 'git rev-parse HEAD'
    git_cmd_res = sub.check_output(git_cmd, shell=True)
    git_hash = git_cmd_res.decode('UTF-8').strip()
    print('Git hash `{!s}` checkout out'.format(git_hash))
    if args.case in ['full', 'radial']:
        for toolchain in toolchains:
            process_toolchain(toolchain, report_prefix=prefix)
    elif args.case == 'specific':
        if any([not os.path.isfile(path) for path in binary_list]):
            raise Exception("Could not find all requested binaries!")
        for binary_path in binary_list:
            __, binary_name = os.path.split(binary_path)
            toolchain = None
            for toolchain in ['pgi', 'intel', 'gcc']:
                if toolchain in binary_name:
                    break
            if toolchain == 'intel':
                ifort_v_res = sub.run('ifort --version', shell=True, stdout=sub.PIPE, stderr=sub.PIPE)
                split = re.split('ifort \(IFORT\) (.+) ', ifort_v_res.stdout.decode('UTF-8'))
                if split[1].startswith('18'):
                    print('Detected ifort18 from environment, using ifort18')
                    toolchain = 'ifort18'
                else:
                    print('No special ifort detected from environment, using ifort12')
            env_cmd = env_commands[toolchain]
            num_trials = trails[jac]
            tests = process_run(env_cmd, num_cores, nrho, repeat, binary_path, num_trials, jac, verbosity=verbosity)
            df = test_restult_to_pandas(tests)
            df.to_csv(prefix + '_{!s}.csv'.format(binary_name))
