# This file is part of QLKNN-fortran
# You should have received the QLKNN-fortran LICENSE in the root of the project
import json
import argparse
import subprocess
import re
from collections import OrderedDict
import sys
import os
from pathlib import Path

try:
    from IPython import embed
except ImportError:
    raise ImportError('Could not find IPython. Try to install with `pip install IPython`')

try:
    import numpy as np
except ImportError:
    raise ImportError('Could not find numpy. Try to install with `pip install numpy`')

try:
    import pandas as pd
except ImportError:
    raise ImportError('Could not find pandas. Try to install with `pip install pandas`')

qlknn_9D_feature_names = [
        "Zeff",
        "Ati",
        "Ate",
        "An",
        "q",
        "smag",
        "x",
        "Ti_Te",
        "logNustar",
    ]

qlknn_jetexp_feature_names = [
        "Ane",
        "Ate",
        "Autor",
        "Machtor",
        "x",
        "Zeff",
        "gammaE",
        "q",
        "smag",
        "alpha",
        "Ani1",
        "Ati0",
        "normni1",
        "Ti_Te0",
        "logNustar",
]

def nn_json_to_namelist_dict(path):
    with open(path) as f:
        j = json.load(f)
    nml = nn_dict_to_namelist_dict(j)
    return nml

def late_fusion_json_to_namelist_dict(path):
    with open(path) as f:
        j = json.load(f)
    nml = late_fusion_dict_to_namelist_dict(j)
    return nml

def nn_dict_to_namelist_dict(nn_dict):
    feature_names = nn_dict.pop('feature_names')
    if len(feature_names) == 9:
        if feature_names != qlknn_9D_feature_names:
            raise ValueError('Feature names have to be {!s} exactly'.format(qlknn_9D_feature_names))
    target_names = nn_dict.pop('target_names')
    if len(target_names) != 1:
        raise ValueError('Only single-output NNs supported')
    name = target_names[0]

    if '_metadata' in nn_dict:
        nn_dict.pop('_metadata')
    n_layers = int(len([key for key in nn_dict if key.startswith('layer')])/2)
    n_hidden_nodes = [0] * (n_layers - 1)
    nml_dict = OrderedDict()
    for layer_type, i_layers in zip(['input', 'hidden', 'output'],
                                   [[1], list(range(2, n_layers)), [n_layers]]):
        for wb in ['weights', 'biases']:
            layerlist = []
            maxsize_layer = [0, 0]
            for i_layer in i_layers:
               layerlist.append(nn_dict.pop(''.join(['layer', str(i_layer), '/', wb, '/Variable:0'])))
               if wb == 'biases' and layer_type in ['input','hidden']:
                   n_hidden_nodes[i_layer-1] = len(layerlist[-1])
               if len(layerlist[-1]) > maxsize_layer[0]:
                   maxsize_layer[0] = len(layerlist[-1])
               if isinstance(layerlist[-1][0],(list,tuple)) and len(layerlist[-1][0]) > maxsize_layer[1]:
                   maxsize_layer[1] = len(layerlist[-1][0])
            if len(layerlist) > 1:
                try:
                    layerlist[0][0][0]
                    nml_multi_layer = np.full((maxsize_layer[0],maxsize_layer[1],len(layerlist)), np.NaN)
                    for ii in range(0,len(layerlist)):
                        nml_multi_layer[:len(layerlist[ii]),:len(layerlist[ii][0]),ii] = layerlist[ii]
                    nml_dict[wb + '_' + layer_type] = nml_multi_layer.tolist()
                except TypeError:
                    nml_multi_layer = np.full((len(layerlist),maxsize_layer[0]), np.NaN)
                    for ii in range(0,len(layerlist)):
                        nml_multi_layer[ii,:len(layerlist[ii])] = layerlist[ii]
                    nml_dict[wb + '_' + layer_type] = nml_multi_layer.T.tolist()
            else:
                nml_dict[wb + '_' + layer_type] = layerlist[0]

    sizes = {'n_hidden_layers': n_layers - 1,
             'n_hidden_nodes': n_hidden_nodes,
             'n_inputs': len(nml_dict['weights_input']),
             'n_outputs': len(nml_dict['weights_output'][0]),
             }

    for fb in ['factor', 'bias']:
        for tf, vars in zip(['target', 'feature'],
                           [target_names, feature_names]):
            lst = [nn_dict['prescale_' + fb][var] for var in vars]
            nml_dict[tf + '_prescale_' + fb] = lst

    nml_dict['hidden_activation'] = nn_dict.pop('hidden_activation')
    return name, nml_dict, sizes

def late_fusion_dict_to_namelist_dict(nn_dict):
    ## ETG net
    #1.common_hidden_layer
    #1.hidden_layer_c1_
    #2.hidden_layer_c1_
    #c1_output

    #1.hidden_layer_c2_efeETG_GB
    #1.hidden_layer_c3_efeETG_GB
    #2.hidden_layer_c2_efeETG_GB
    #2.hidden_layer_c3_efeETG_GB
    #c2_output_efeETG_GB
    #c3_output_efeETG_GB

    ## ITG net
    #1.common_hidden_layer
    #1.hidden_layer_c1_
    #2.hidden_layer_c1_
    #c1_output

    #1.hidden_layer_c2_efeITG_GB
    #2.hidden_layer_c2_efeITG_GB
    #2.hidden_layer_c3_efeITG_GB
    #1.hidden_layer_c3_efeITG_GB

    #1.hidden_layer_c2_efiITG_GB
    #2.hidden_layer_c2_efiITG_GB
    #2.hidden_layer_c3_efiITG_GB
    #1.hidden_layer_c3_efiITG_GB

    #1.hidden_layer_pfeITG_GB
    #2.hidden_layer_pfeITG_GB

    #c2_output_efeITG_GB
    #c2_output_efiITG_GB
    #c3_output_efeITG_GB
    #c3_output_efiITG_GB

    #output_pfeITG_GB

    # Drop empty layers
    layer_dict = nn_dict['weights']
    for layer_name in list(layer_dict.keys()):
        if len(layer_dict[layer_name]) == 0:
            del layer_dict[layer_name]

    common_layer = layer_dict.pop('1.common_hidden_layer')

    pfe_in_layer = None
    for layer_name in list(layer_dict.keys()):
        if 'pfe' in layer_name and layer_name.startswith('1'):
            pfe_in_layer = layer_dict.pop(layer_name)
            break

    # Find outputs
    #outputs = set()
    #for layer_name in list(layer_dict.keys()):
    #    split = layer_name.split('_')
    #    if split[-1] == 'GB':
    #        outputs.add('_'.join(split[-2:]))

    # Find sizes of the hidden layers
    layersizes = {}
    for layer_name in list(layer_dict.keys()):
        if 'hidden_layer' in layer_name:
            split = layer_name.split('.')
            group_name = split[1][13:]
            # Treat the first pfe layer as an input layer. It is differently shaped
            if 'pfe' in group_name and split[0] == '1':
                pass
            elif group_name not in layersizes:
                layersizes[group_name] = {
                    'n_hidden_layers': 1,
                    'n_hidden_nodes': len(layer_dict[layer_name][0]),
                }
            else:
                layersizes[group_name]['n_hidden_layers'] += 1

    # Get hidden layers from Keras JSON
    layers = {}
    for layer_name in list(layer_dict.keys()):
        if 'hidden_layer' in layer_name:
            split = layer_name.split('.')
            group_name = split[1][13:]
            weights = np.array(layer_dict[layer_name][0])
            biases = np.array(layer_dict.pop(layer_name)[1])
            num_layers = layersizes[group_name]['n_hidden_layers']
            if 'pfe' not in group_name:
                layernumber = int(split[0]) - 1
                if group_name not in layers:
                    layers[group_name] = {
                        'weights_hidden': np.full((weights.shape[0], weights.shape[1], num_layers), np.nan),
                        'biases_hidden': np.full((biases.shape[0], num_layers), np.nan),
                        'hidden_activation': ['tanh'] * num_layers
                    }
                layers[group_name]['weights_hidden'][:, :, layernumber] = weights
                layers[group_name]['biases_hidden'][:, layernumber] = biases
            # Treat the first pfe layer as an input layer. It is differently shaped
            elif split[0] != '1':
                layernumber = int(split[0]) - 2
                if group_name not in layers:
                    layers[group_name] = {
                        'weights_hidden': np.full((weights.shape[0], weights.shape[1], num_layers), np.nan),
                        'biases_hidden': np.full((biases.shape[0], num_layers), np.nan),
                        'hidden_activation': ['tanh'] * num_layers,
                    }
                layers[group_name]['weights_hidden'][:, :, layernumber] = weights
                layers[group_name]['biases_hidden'][:, layernumber] = biases

    for group_name in list(layersizes.keys()):
        split = group_name.split('_')
        if 'pfe' not in group_name:
            out_name = '_'.join([split[0], 'output'] + split[1:]).rstrip('_')
        else:
            out_name = 'output_' + group_name
        layers[group_name]['weights_output'] = layer_dict[out_name][0]
        layers[group_name]['biases_output'] = layer_dict[out_name][1]
        layersizes[group_name]['n_outputs'] = len(layer_dict[out_name][1])

    # Save "input layer net"
    #layers['inp_layer'] = {}
    #layers['inp_layer']['weights_input'] = common_layer[0]
    #layers['inp_layer']['biases_input'] = common_layer[1]
    #layersizes['inp_layer'] = {}
    #layersizes['inp_layer']['n_inputs'] = len(common_layer[0])


    for group_name in list(layersizes.keys()):
        if 'ETG' in group_name:
            nn_type = 'ETG'
            break
        elif 'TEM' in group_name:
            nn_type = 'TEM'
            break
        elif 'ITG' in group_name:
            nn_type = 'ITG'
            break

    if nn_type == 'ETG':
        target_names = pd.Series(['efeETG_GB'])
        branch1_names = ['Zeff', 'Ati', 'An', 'q', 'smag', 'x', 'Ti_Te', 'logNustar']
        branch2_names = ['Ate']
        feature_names = ['Zeff', 'Ati', 'Ate', 'An', 'q', 'smag', 'x', 'Ti_Te', 'logNustar']
    elif nn_type == 'ITG':
        target_names = pd.Series(['efeITG_GB', 'efiITG_GB', 'pfeITG_GB'])
        branch1_names = ['Zeff', 'Ate', 'An', 'q', 'smag', 'x', 'Ti_Te', 'logNustar']
        branch2_names = ['Ati']
        feature_names = ['Zeff', 'Ati', 'Ate', 'An', 'q', 'smag', 'x', 'Ti_Te', 'logNustar']
    elif nn_type == 'TEM':
        target_names = pd.Series(['efeTEM_GB', 'efiTEM_GB', 'pfeTEM_GB'])
        branch1_names = ['Zeff', 'Ati', 'An', 'q', 'smag', 'x', 'Ti_Te', 'logNustar']
        branch2_names = ['Ate']
        feature_names = ['Zeff', 'Ati', 'Ate', 'An', 'q', 'smag', 'x', 'Ti_Te', 'logNustar']


    fusion_blocks = {
        'target_prescale_factor': [nn_dict['prescale_factor'][k] for k in target_names],
        'target_prescale_bias': [0] * len(target_names),
        'feature_prescale_factor': [nn_dict['prescale_factor'][k] for k in feature_names],
        'feature_prescale_bias': [nn_dict['prescale_bias'][k] for k in feature_names],
    }

    layers['c1_' + nn_type] = layers.pop('c1_')
    layersizes['c1_' + nn_type] = layersizes.pop('c1_')
    ## Flatten layersizes to 'sizes' style dict
    #sizes = {}
    #for group_name in list(layersizes.keys()):
    #    for size in list(layersizes[group_name].keys()):
    #        sizes[group_name + '_' + size] = layersizes[group_name].pop(size)
    #    if len(layersizes[group_name]) == 0:
    #        layersizes.pop(group_name)

    ## Flatten layers to 'nml_dict' style
    #for group_name in list(layers.keys()):
    #    for size in list(layers[group_name].keys()):
    #        nml_dict[group_name + '_' + size] = layers[group_name].pop(size)
    #    if len(layers[group_name]) == 0:
    #        layers.pop(group_name)

    #if len(layersizes) != 0:
    #    raise Exception('Did not flatten all layersizes, {!s} is left.'.format(layersizes.keys()))
    #if len(layers) != 0:
    #    raise Exception('Did not flatten all layers, {!s} is left.'.format(layers.keys()))
    fusion_blocks['weights_input'] = common_layer[0]
    fusion_blocks['biases_input'] = common_layer[1]
    fusion_blocksizes = {}
    fusion_blocksizes['n_inputs'] = len(common_layer[0])
    fusion_blocksizes['n_hidden_nodes'] = len(common_layer[1])
    fusion_blocksizes['n_target_prescale'] = len(fusion_blocks['target_prescale_factor'])
    fusion_blocksizes['n_feature_prescale'] = len(fusion_blocks['feature_prescale_factor'])

    pfe_blocks = {}
    pfe_blocksizes = {}

    if nn_type == 'ETG' and pfe_in_layer is not None:
        raise Exception('Found pfe layer for ETG net! Should not be')
    elif (nn_type == 'ITG' or nn_type == 'TEM'):
        if pfe_in_layer is None:
            raise Exception('Found pfe layer not found for ITG/TEM net!')
        else:
            pfe_blocks['weights_input'] = pfe_in_layer[0]
            pfe_blocks['biases_input'] = pfe_in_layer[1]
            pfe_blocksizes = {}
            pfe_blocksizes['n_inputs'] = len(pfe_in_layer[0])
            pfe_blocksizes['n_hidden_nodes'] = len(pfe_in_layer[1])
            pfe_blocksizes['n_target_prescale'] = 0
            pfe_blocksizes['n_feature_prescale'] = 0
    return nn_type, layers, layersizes, fusion_blocks, fusion_blocksizes, pfe_blocks, pfe_blocksizes

def fusion_nml_dict_to_namelist(target_path, layers, sizes, fusion_scaling):
    np.set_printoptions(threshold=np.inf)
    init_strings = []
    declare_strings = []
    with open(target_path, 'w') as f:
        f.write('&sizes\n')
        for key, val in sizes.items():
            f.write('    ' + key + ' = ' + str(val) + '\n')
        f.write('/\n\n')

    #for key, val in nml_dict.items():
    #    if isinstance(val, list):
    #        init_str = array_to_namelist_string(key, np.array(val))
    #        init_strings.append(init_str)
    #        #declare_strings.append(declare_str)
    #    else:
    #        print('Do not know what to do with {!s}'.format(key))
    #        embed()
    #        raise Exception
    ##nml_dict['weights_input'] = np.asfortranarray(nml_dict['weights_input']).tolist()
    ##nml_dict['biases_hidden'] = np.asfortranarray(nml_dict['biases_hidden']).T.tolist()
    ##arr = np.array(nml_dict['weights_hidden'])
    ##init_str = array_to_namelist_string('weights_hidden', arr)
    #print('Writing to', target_path)
    #with open(target_path, 'a') as f:
    #    f.write('&netfile\n')
    #    #f.writelines([el + '\n' for el in declare_strings])
    #    #f.write('\n')
    #    f.writelines(['    ' + el + '\n' for el in init_strings])
    #    f.write('/\n')
    #embed()

def nml_dict_to_namelist(target_path, nml_dict, sizes):
    #nml['sizes'] = sizes
    np.set_printoptions(threshold=np.inf, nanstr='')
    init_strings = []
    declare_strings = []
    node_vector_flag = False
    with open(target_path, 'w') as f:
        f.write('&sizes\n')
        for key, val in sizes.items():
            if key == 'n_hidden_nodes' and isinstance(val, (list, np.ndarray)):
                temp = np.array(val)
                if np.all(temp[:-1] == temp[1:]):
                    f.write('    ' + key + ' = ' + str(val[0]) + '\n')
                else:
                    node_vector_flag = True
            else:
                f.write('    ' + key + ' = ' + str(val) + '\n')
        f.write('/\n\n')
        if node_vector_flag:
            f.write('&nodes\n')
            newval = array_to_namelist_string('n_hidden_node_vector', np.array(sizes['n_hidden_nodes']))
            f.write('    ' + newval)
            f.write('/\n\n')
    for key, val in nml_dict.items():
        if isinstance(val, (list, np.ndarray)):
            init_str = array_to_namelist_string(key, np.array(val))
            init_strings.append(init_str)
            #declare_strings.append(declare_str)
        else:
            print('Do not know what to do with {!s}'.format(key))
            embed()
            raise Exception
    #nml_dict['weights_input'] = np.asfortranarray(nml_dict['weights_input']).tolist()
    #nml_dict['biases_hidden'] = np.asfortranarray(nml_dict['biases_hidden']).T.tolist()
    #arr = np.array(nml_dict['weights_hidden'])
    #init_str = array_to_namelist_string('weights_hidden', arr)
    print('Writing to', target_path)
    with open(target_path, 'a') as f:
        f.write('&netfile\n')
        #f.writelines([el + '\n' for el in declare_strings])
        #f.write('\n')
        f.writelines(['    ' + el + '\n' for el in init_strings])
        f.write('/\n')
    return True

type_map = {
    'float64': 'REAL',
    '<U4': 'CHARACTER(len={!s})',
}
stringlike = ['<U4']
def array_to_namelist_string(varname, array, type='REAL', precision=15):
    if varname.startswith('weights_'):
        array = array.swapaxes(0,1)
    init_str = ''
    np.set_printoptions(threshold=np.inf, nanstr='')
    line_to_string = lambda line: np.array2string(line, max_line_width=np.inf, separator=', ', formatter={'str_kind': lambda x: '"' + x + '"'}, precision=precision).replace('[', '').replace(']', '')
    if array.ndim == 1:
        linestr = line_to_string(array)
        init_str += varname + ' = ' + linestr + '\n'
    elif array.ndim == 2:
        for column in range(array.shape[1]):
            linestr = line_to_string(array[:, column])
            init_str += varname + '(:, ' + str(column + 1) + ') = ' + linestr + '\n'
    elif array.ndim == 3:
        for page in range(array.shape[2]):
            for column in range(array.shape[1]):
                linestr = line_to_string(array[:, column, page])
                init_str += varname + '(:, ' + str(column + 1) + ', ' + str(page + 1) + ') = ' + linestr + '\n'
    return init_str

def array_to_string(varname, array, type='REAL', precision=15):
    if varname.startswith('weights_'):
        array = array.swapaxes(0,1)
    init_str = ''
    if array.ndim > 1:
        largest_dim = np.argmax(array.shape)
        for ii in range(array.shape[largest_dim]):
            slicer = [slice(None)] * array.ndim
            slicer[largest_dim] = ii
            subarray = array[tuple(slicer)]
            val_str = np.array2string(np.ravel(subarray, 'F'), separator=', ', formatter={'str_kind': lambda x: '"' + x + '"'}, precision=precision)
            val_str = val_str.replace('[', '(/').replace(']', '/)')
            val_str = val_str.replace('\n', ' &\n        ')
            slice_str = [':,'] * array.ndim
            slice_str[largest_dim] = str(ii + 1) + ','
            if slice_str[-1].endswith(','):
                slice_str[-1] = slice_str[-1][:-1]

            if subarray.ndim > 1:
                shape_str2 = str(subarray.shape)
                shape_str2 = shape_str2.replace('(', '(/').replace(')', '/)')
                init_str += ''.join(['        ' + varname + '('] + slice_str + [') = RESHAPE(']) + val_str + ', ' + shape_str2 + ')\n'
            else:
                init_str += ''.join(['        ' + varname + '('] + slice_str + [') = ']) + val_str + '\n'
    #init_str = np.array2string(np.ravel(array, 'F'), separator=', ', formatter={'str_kind': lambda x: '"' + x + '"'})
    #init_str = init_str.replace('[', '(/').replace(']', '/)')
    #init_str = init_str.replace('\n', ' &\n        ')
    #if len(array.shape) > 1:
    #    shape_str = str(array.shape)
    #    shape_str = shape_str.replace('(', '(/').replace(')', '/)')
    #    init_str = ''.join([' '*8, 'RESHAPE (',init_str, ', ', shape_str, ')'])
    #    shape_str2 = str(array.shape)
    #    shape_str2 = shape_str2.replace('(', '').replace(')', '')
    #else:
    #    shape_str2 = str(len(array))
        shape_str = str(array.shape)
        shape_str = shape_str.replace('(', '').replace(')', '')
    else:
        shape_str = str(len(array))
        val_str = np.array2string(np.ravel(array, 'F'), separator=', ', formatter={'str_kind': lambda x: '"' + x + '"'})
        val_str = val_str.replace('[', '(/').replace(']', '/)')
        val_str = val_str.replace('\n', ' &\n        ')
        init_str = varname + ' = ' + val_str + '\n'
    if str(array.dtype) in stringlike:
        arr_type = type_map[str(array.dtype)].format(max(np.vectorize(lambda x: len(x))(array)))
    else:
        arr_type = type_map[str(array.dtype)]
    decl_str = '    ' + arr_type + ', DIMENSION(' + shape_str + ') :: ' + varname
    #+ ' = init_' + varname
    return init_str, decl_str

def array_init_to_func(varname, init_str):
    init_func =  '  subroutine init_' + varname + '()\n'
    init_func += init_str
    init_func += '  end subroutine'
    return init_func

def build_init_all(varnames):
    init_all =  '  subroutine init_all()\n'
    for varname in varnames:
        init_all += '    call init_' + varname + '()\n'
    init_all += '  end subroutine init_all\n'
    return init_all

def nml_dict_to_source(name, nml_dict, target_dir='../src'):
    np.set_printoptions(threshold=np.inf)
    strings = OrderedDict()
    for key, val in nml_dict.items():
        if isinstance(val, list):
            init_str, decl_str = array_to_string(key, np.array(val))
            strings[key] = (init_str, decl_str)
        else:
            print('Do not know what to do with {!s}'.format(key))
            embed()
            raise Exception

    out_path = os.path.join(target_dir, 'net_' + name.lower() + '.f90')
    print('Writing to', out_path)
    with open(out_path, 'w') as f:
        f.write(module_start.format(name.lower()))
        f.writelines([el[1] + '\n' for el in strings.values()])
        f.write('contains\n')
        for varname, (init_str, decl_str) in strings.items():
            init_func = array_init_to_func(varname, init_str)
            f.write(init_func + '\n')
        #f.write('\n')
        #f.writelines(['    ' + el + '\n' for el in declare_strings])
        f.write(build_init_all(strings.keys()))
        f.write(module_end.format('get_' + name.lower()))
        f.write('end module net_' + name.lower())
module_start = \
"""
module net_{0!s}
  use qlknn_types
"""
module_end = \
"""
  function {0!s}()
    type(networktype) :: {0!s}
    call init_all()
    {0!s}%weights_input =           weights_input
    {0!s}%biases_input =            biases_input
    {0!s}%biases_hidden =           biases_hidden
    {0!s}%weights_hidden =          weights_hidden
    {0!s}%weights_output =          weights_output
    {0!s}%biases_output =           biases_output

    {0!s}%hidden_activation =       hidden_activation

    {0!s}%target_prescale_bias =    target_prescale_bias
    {0!s}%target_prescale_factor =  target_prescale_factor
    {0!s}%feature_prescale_bias =   feature_prescale_bias
    {0!s}%feature_prescale_factor = feature_prescale_factor
  end function {0!s}
"""


def get_depth(path):
    return len(path.parents) - 1


def convert_all(path, target_dir='../data', target='namelist', verbosity=0, force=False, depth=0):
    path = Path(path)
    target_dir = Path(target_dir)
    if not target_dir.is_dir():
        raise OSError(f"Target '{target_dir}' is not a folder or does not exists. Abort")
    if path.is_dir():
        if len(os.listdir(path)) == 0:
            raise OSError(f"Path '{target_dir}' is empty!")

        # Find JSONs within depth
        jsons = (p.relative_to(path) for p in path.rglob("*.json"))
        jsons = [p for p in jsons if get_depth(p) <= depth]
        if verbosity >= 1:
            print(f"Found {len(jsons)} jsons within depth {depth}")
        if verbosity >= 2:
            print(jsons)
        # Make name Path map
        json_map = {"-".join(p.parts): p for p in jsons}
        nml_map = {}
        for prename, p in json_map.items():
            filepath = path / p
            assert filepath.is_file()
            assert filepath.suffix == ".json"
            json_mtime = filepath.stat().st_mtime
            target_name, nml_dict, sizes = nn_json_to_namelist_dict(filepath)
            name = filepath.stem
            if target == 'source':
                if verbosity >= 1:
                    print('Converting', filepath)
                nml_dict_to_source(name, nml_dict, target_dir=target_dir)
            elif target == 'namelist':
                target_stem = f"net_{target_name.lower()}"
                if target_stem in nml_map:
                    # If this is the first time a target is encountered
                    n_nets = len(nml_map[target_stem])
                    if n_nets == 1:
                        # First time a duplicate appears
                        old_path = nml_map[target_stem][0][1]
                        new_target_stem = target_stem + f"_{n_nets:02d}"
                        target_path = target_dir / (new_target_stem + ".nml")
                        if verbosity >= 1:
                            print(f"Detected duplicate target {target_stem}, moving previous net to {target_path.stem}")
                        new_path = old_path.rename(target_path)
                        # Remake nml_map
                        del nml_map[target_stem][0]
                        net_tuple = (new_target_stem, new_path)
                        nml_map[target_stem] = [(net_tuple + (True, ))]
                        del new_target_stem, new_path, target_path

                    new_target_stem = target_stem + f"_{n_nets+1:02d}"
                    target_path = target_dir / (new_target_stem + ".nml")
                    net_tuple = (new_target_stem, target_path)
                else:
                    # If this target was here before
                    target_path = target_dir / (target_stem + ".nml")
                    net_tuple = (target_stem, target_path)


                if target_path.exists() and json_mtime < target_path.stat().st_mtime and not force:
                    if verbosity >= 1:
                        print('Skipping {!s}, already up to date'.format(filepath))
                        success = True
                else:
                    if verbosity >= 1:
                        print('Converting', filepath)
                    success = nml_dict_to_namelist(target_path, nml_dict, sizes)
                if target_stem not in nml_map:
                    nml_map[target_stem] = [(net_tuple + (success, ))]
                else:
                    nml_map[target_stem].append((net_tuple + (success, )))
                del target_stem, net_tuple, success
            else:
                raise ValueError('Unknown target {!s}'.format(target))
    else:
        raise OSError(f"Given path '{target_dir}' is not a folder or does not exists. Abort")

    # Check if all nets are there
    nets = [net for target in nml_map.values() for net in target]
    for net in nets:
        name, path, sucess = net
    assert len(nets) == len(json_map), "Not every JSON has a matching namelist!"

def convert_late_fusion_nets(path, target_dir='../data', verbosity=0):
    if not os.path.isdir(target_dir):
        raise OSError("Target folder '{!s}' does not exists. Abort".format(target_dir))
    if not os.path.isdir(path):
        raise OSError("Source folder '{!s}' does not exists. Abort".format(path))
    if len(os.listdir(path)) == 0:
        raise OSError("Source folder '{!s}' empty. Abort!".format(path))
    CGM_dirs = ['ETG', 'ITG', 'TEM']
    CGM_nn_files = [os.path.join(path, folder, 'nn.json') for folder in CGM_dirs]
    if any(not os.path.isfile(file) for file in CGM_nn_files):
        raise OSError('Could not find all directories required in {!s}'.format(path))
    for file in CGM_nn_files:
        nn_type, layers, layersizes, fusion_blocks, fusion_blocksizes, pfe_blocks, pfe_blocksizes = late_fusion_json_to_namelist_dict(file)
        for group_name in layersizes.keys():
            target_path = os.path.join(target_dir, group_name.lower() + '.nml')
            nml_dict_to_namelist(target_path, layers[group_name], layersizes[group_name])
        target_path = os.path.join(target_dir, nn_type.lower() + '.nml')
        nml_dict_to_namelist(target_path, fusion_blocks, fusion_blocksizes)
        if len(pfe_blocks) != 0:
            target_path = os.path.join(target_dir, nn_type.lower() + '_pfe.nml')
            nml_dict_to_namelist(target_path, pfe_blocks, pfe_blocksizes)

def main():
    parser = argparse.ArgumentParser(description='Tools for namelist conversions.')
    parser.add_argument('json_dir', type=str, help='Path to the folder containing the QLKNN JSONs')
    parser.add_argument('target_dir', type=str, help='Path to the folder where the namelists should be created in')
    parser.add_argument("--verbosity", "-v", default=0, action="count")
    parser.add_argument("--depth", "-d", type=int, default=0)
    parser.add_argument('--force', "-f", action='store_true', help='Force overwrite existing namelists')
    parser.add_argument('--target', type=str, help='Set type of NN files to be generated "namelist" or "source"', default='namelist')
    parser.add_argument('--late_fusion', action='store_true', help='Generate namelists for fusion net, pass standardization file')
    args = parser.parse_args()

    #name, nml_dict, sizes = nn_json_to_namelist_dict('./test_nn.json')
    #src_str = nml_dict_to_source(name, nml_dict)
    #nml_dict_to_namelist(name, nml_dict, sizes, target_dir='.')
    if args.late_fusion:
        convert_late_fusion_nets(args.json_dir, target_dir=args.target_dir, verbosity=args.verbosity)
    else:
        convert_all(args.json_dir, target_dir=args.target_dir, verbosity=args.verbosity, force=args.force, target=args.target, depth=args.depth)


if __name__ == '__main__':
    main()
