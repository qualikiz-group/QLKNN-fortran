function [] = write_namelist(arr, filename)
    % write_namelist  Writes a QLKNN-style Fortran namelist.
    % Assumes the passed array is in "RAPTOR style", namely
    % as n_rho rows and n_inp columns. 
    fileID = fopen(filename, 'w');
    fprintf(fileID, '&sizes\n');
    fprintf(fileID, '    n_in = %i\n', size(arr, 2));
    fprintf(fileID, '    n_rho = %i\n', size(arr, 1));
    fprintf(fileID, '/\n\n');
    fprintf(fileID, '&test\n');

    for ii = 1:size(arr, 1)
        fprintf(fileID, '    input(:, %i) = ', ii);
    	fprintf(fileID, strcat(repmat('%12.8f ', 1, size(arr, 2)), '\n'), arr(ii, :));
    end
    fprintf(fileID, '/\n');

    fclose(fileID);
end