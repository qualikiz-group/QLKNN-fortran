#!/usr/bin/env bash
# This file is part of QLKNN-fortran
# You should have received the QLKNN-fortran LICENSE in the root of the project
#
# Run a binary multiple times, to be interpreted by gprof
# As such, compile your binaries with `-gp`!
#
# Author: Karel van de Plassche <karelvandeplassche@gmail.com>
# Released under MIT License (See LICENSE)
set -u

function usage() {
  echo ""
  echo "Usage: `basename $0` [-t <trails>] <binary> [<binary flags>]"
  echo ""
  echo "  <binary>       Path to a binary to gprof"
  echo "  <binary flags> Flags to pass to binary (unprocessed)"
  echo "  -h             Print this usage information and exit"
  echo "  -t <trials>    Number of trials to run"
  echo ""
}

while getopts ":t:" opt; do
  case $opt in
    h)
      usage
      exit 0
      ;;
    t)
      trials="$OPTARG"
      ;;
    \?)
      echo "Invalid option: -$OPTARG" >&2
      usage
      exit 1
      ;;
    :)
      echo "Option -$OPTARG requires an argument." >&2
      usage
      exit 1
      ;;
  esac
done
shift $((OPTIND-1))

function run_binaries() {
  profile_files=()
  # https://stackoverflow.com/a/169517/3613853 'seq' is nice
  for ii in $(seq 1 $trials); do
    set -x
    $binary $binary_flags
    { set +x; } 1>/dev/null
    profile_file=gmon$ii.out
    mv gmon.out $profile_file
    profile_files[$ii]=$profile_file
  done
  profile_files=$( IFS=$'\n'; echo "${profile_files[*]}" )
}

exit_on_error() {
    exit_code=$1
    last_command=${@:2}
    if [ $exit_code -ne 0 ]; then
        >&2 echo "\"${last_command}\" command failed with exit code ${exit_code}."
        exit $exit_code
    fi
}


if [ $# -lt 1 ]; then
  echo "Missing path to binary." >&2
  usage
  exit 1
fi

binary="$1"
binary_flags="${@:2}"

# Do not clean up for now
# trap cleanup EXIT

# Reroute set -x info to stdout
export BASH_XTRACEFD=1

echo "#######################"
echo "# Starting binary runs"
echo "#######################"
run_binaries

echo "#######################"
echo "# Report by gprof"
echo "#######################"

set -x
gprof -P $binary $profile_files
{ set +x; } 1>/dev/null
