# This file is part of QLKNN-fortran
# You should have received the QLKNN-fortran LICENSE in the root of the project
import argparse
import os
import subprocess
from namelist_tools import generate_input_namelist, qlknn_9D_feature_names
from pathlib import Path

from matplotlib.backends.backend_pdf import PdfPages
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

from IPython import embed
import f90nml


def process_single_stepsize(casenum, nettype, stepsize, verbosity=0):
    # We assume Jacobians are not compiler nor build dependent....

    # Generate midpoint `x` for jacobians
    nrho = 24
    input_path = 'prof_in.nml'
    output_path = Path('jac_result.nml')
    generate_input_namelist(qlknn_9D_feature_names + ['gammaE', 'Te'],
                            target_path=input_path, scann=nrho)

    # Find any binary we can find
    bin_dir = Path('./bin')
    for file in bin_dir.iterdir():
        if file.name.startswith('qlknn_jacobian_test_cli-'):
            binary_path = file
            break

    cmd_verbosity = max(verbosity - 1, 0) + 1
    cmd = [
        binary_path,
        #f"--nn-path="
        f"--input={input_path}",
        f"--output={output_path}",
        f"--nettype={nettype}",
        f"--casenum={casenum}",
        f"--stepsize={stepsize}",
        f"--verbosity={cmd_verbosity}",
    ]
    if verbosity >= 1:
        print('Running {!s}'.format(' '.join(cmd)))
    try:
        outp = subprocess.check_output(cmd, stderr=subprocess.STDOUT)
    except subprocess.CalledProcessError as ee:
        print(ee.stdout.decode('UTF-8'))
        raise

    if output_path.exists():
        # Do not parse the full file, it'll take a while. Instead, parse stdout
        # res = f90nml.read(output_path)
        string_outp = [line.decode('UTF-8') for line in outp.splitlines()]
        max_err = None
        for line in string_outp:
            if line.startswith("Maximum error was"):
                split = line.split()
                max_err = float(split[-1])

        if max_err is None:
            raise Exception('Could not determine max error from stdout')
    else:
        raise Exception('Did not generate output file {output_path}')

    if verbosity >= 0:
        print(f"Max error for stepsize {stepsize} was {max_err}")

    return max_err


def process_case(casenum, nettype, verbosity=0):
    if verbosity >= 0:
        print(f"Processing case {casenum}")
    logmin = -7
    logmax = -1
    nsteps = logmax - logmin + 1
    results = []
    for stepsize in np.logspace(logmin, logmax, num=nsteps):
        max_err = process_single_stepsize(casenum, nettype, stepsize, verbosity)
        results.append((stepsize, max_err))
    df = pd.DataFrame(results, columns=['stepsize', 'max_err'])
    return df


def get_max_errors_from_source():
    max_errs = {}
    jac_source = Path('tests/test_jacobian.f90')
    with jac_source.open() as f_:
        for line in f_:
            if ':: max_err_hyper =' in line:
                split = line.split('=')
                farray = split[-1].strip()
                arrstr = farray.lstrip('(/').rstrip('/)').strip()
                max_errs['hyper'] = np.fromstring(arrstr, sep=',')
            if ':: max_err_hornnet =' in line:
                split = line.split('=')
                farray = split[-1].strip()
                arrstr = farray.lstrip('(/').rstrip('/)').strip()
                max_errs['hornnet'] = np.fromstring(arrstr, sep=',')
                break
    if 'hyper' not in max_errs:
        raise Exception('Could not read hyper max_errs from file')
    if 'hornnet' not in max_errs:
        raise Exception('Could not read hornnet max_errs from file')
    return max_errs


def get_stepsize_from_source():
    stepsizes = {}
    jac_source = Path('tests/test_jacobian.f90')
    with jac_source.open() as f_:
        for line in f_:
            if ':: stepsize_hyper =' in line:
                split = line.split('=')
                farray = split[-1].strip()
                arrstr = farray.lstrip('(/').rstrip('/)').strip()
                stepsizes['hyper'] = np.fromstring(arrstr, sep=',')
            if ':: stepsize_hornnet =' in line:
                split = line.split('=')
                farray = split[-1].strip()
                arrstr = farray.lstrip('(/').rstrip('/)').strip()
                stepsizes['hornnet'] = np.fromstring(arrstr, sep=',')
                break
    if 'hyper' not in stepsizes:
        raise Exception('Could not read hyper max_errs from file')
    if 'hornnet' not in stepsizes:
        raise Exception('Could not read hornnet max_errs from file')
    return stepsizes


def generate_report(caselist, nettype='hyper', output_path='jacobians.pdf', verbosity=0):
    max_allowed_errors = get_max_errors_from_source()
    proposed_stepsize = get_stepsize_from_source()
    pdf_collection = PdfPages(output_path)
    for case in caselist:
        df = process_case(case, nettype)

        fig, ax = plt.subplots()
        df.plot(ax=ax, loglog=True, x='stepsize', y='max_err', marker="o", legend=False)

        formula = (r"$ E \equiv \frac{df}{d\mathbf{x}}(\mathbf{x}) * h - \left[ f(\mathbf{x}+\frac{h}{2}) - f(\mathbf{x}-\frac{h}{2}) \right] $" + "\n"
                   r"$ E_N \equiv E / f(\mathbf{x}-\frac{h}{2}) $ ")
        ax.hlines(max_allowed_errors[nettype][int(case) - 1], ax.get_xlim()[0], ax.get_xlim()[1],
                  linestyle='--', color='black', alpha=0.5)
        ax.vlines(proposed_stepsize[nettype][0], ax.get_ylim()[0], ax.get_ylim()[1],
                  linestyle='--', color='black', alpha=0.5)
        ax.set_xlabel(r"Stepsize $h$")
        ax.set_ylabel(r"Normalized error $E_N$")
        ax.set_title(formula)
        fig.suptitle(f"Case = {case}, nettype = {nettype}")
        fig.tight_layout()
        pdf_collection.savefig(fig)
    pdf_collection.close()


if __name__ == '__main__':
    start_dir = os.path.abspath(os.getcwd())
    this_dir = os.path.dirname(os.path.abspath(__file__))
    os.chdir(os.path.join(this_dir, '..'))
    verbosity = 0

    parser = argparse.ArgumentParser(description='Tools for binary profiling.')
    parser.add_argument('caselist', type=str,
                        help='Comma-separated list of cases to be checked',
                        nargs='?', default=None)
    parser.add_argument('nettype', type=str,
                        help='Name of the NN to be ran',
                        nargs='?', default='hyper')
    args = parser.parse_args()

    if args.caselist is None or args.caselist == 'all':
        if args.nettype == 'hyper':
            caselist = [1, 2, 3, 4, 5]
        elif args.nettype == 'hornnet':
            caselist = [1, 2, 3, 4, 5, 6]
    else:
        caselist = args.caselist.split(',')

    generate_report(caselist, nettype=args.nettype)
