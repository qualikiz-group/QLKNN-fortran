# This file is part of QLKNN-fortran
# You should have received the QLKNN-fortran LICENSE in the root of the project
import argparse
from collections import OrderedDict

import pandas as pd
import numpy as np
from IPython import embed
from matplotlib.backends.backend_pdf import PdfPages
import matplotlib.pyplot as plt
import os


def process_frame(df, prefix):
    numcore_opts = df.index.get_level_values('num_cores').unique()
    nrho_opts = df.index.get_level_values('nrho').unique()
    if prefix == 'bench_radial':
        if len(numcore_opts) > 1:
            print('Warning! Found numcore={!s}, choosing {!s}'.format(numcore_opts.values, numcore_opts[0]))
            df = df.loc[(slice(None), df.index.get_level_values('num_cores') == numcore_opts[0], slice(None)), :]
    elif prefix == 'bench_result':
        if len(nrho_opts) > 1:
            print('Warning! Found nrho={!s}, choosing {!s}'.format(numcore_opts.values, numcore_opts[0]))
            df = df.loc[(df.index.get_level_values('nrho') == nrho_opts[0], slice(None), slice(None)), :]
    std = df.std(level=['nrho', 'num_cores'])
    std.columns = ['std_' + col for col in std.columns]
    median = df.median(level=['nrho', 'num_cores'])
    median.columns = ['median_' + col for col in median.columns]
    summary = pd.concat([median, std], axis=1)

    stacked = df.unstack(['nrho', 'num_cores'])
    if prefix == 'bench_radial':
        stacked.columns = stacked.columns.droplevel('num_cores')
        summary.index = summary.index.droplevel('num_cores')
    elif prefix == 'bench_result':
        stacked.columns = stacked.columns.droplevel('nrho')
        summary.index = summary.index.droplevel('nrho')
    elif prefix == 'bench_bin':
        stacked.columns = stacked.columns.droplevel('nrho')

    if prefix in ['bench_radial', 'bench_result']:
        stacked.columns = stacked.columns.droplevel(0)
    else:
        stacked.columns = df.columns
    return stacked, summary

def generate_report(folder_list, root='.', prefix='bench_bin', output_path='report.pdf', plot_linear_scaling=True):
    pdf_collection = PdfPages(output_path)
    root = os.path.abspath(root)
    versions = [file[:-4] for file in os.listdir(os.path.join(root, folder_list[0])) if file.endswith('.csv') and file.startswith(prefix)]
    if len(versions) == 0:
        raise Exception('No versions with prefix {!s} found in {!s}'.format(prefix, folder_list[0]))
    stacked_dict = OrderedDict()
    if prefix in ['bench_radial', 'bench_result']:
        for version in versions:
            fig, ax = plt.subplots()
            ax.set_xlabel('num cores')
            ax.set_ylabel('runtime per trial [s]')
            ax.set_title(version)
            for folder in folder_list:
                folder_path = os.path.join(root, folder)
                if not os.path.isdir(folder_path):
                    raise Exception('Could not find folder {!s}, abort.'.format(folder_path))

                file = os.path.join(folder_path, version + '.csv')
                if not os.path.isfile(file):
                    raise Exception('Could not find version {!s} in folder {!s}, abort.'.format(version, folder_path))

                df = pd.read_csv(file, index_col=[0, 1, 2])
                stacked, summary = process_frame(df, prefix)

                if prefix == 'bench_radial':
                    linear_scale =  (summary.index / summary.index[0]) * summary['median_runtime'].iloc[0]
                else:
                    linear_scale = summary['median_runtime'].iloc[0] / (summary.index / summary.index[0])

                line, = ax.plot(summary.index, summary['median_runtime'], alpha=0.5, label=folder)
                color = line.get_color()
                if plot_linear_scaling:
                    ax.plot(summary.index, linear_scale, label='linear scaling', linestyle='--', alpha=0.3, color=color)
                bp = stacked.plot.box(ax=ax,
                                      flierprops={'markeredgecolor': color},
                                      return_type='dict',
                                      positions=stacked.columns.get_level_values(0).values,
                                      widths=min(np.diff(stacked.columns)),
                                      )
                #for thing in ['medians', 'caps', 'boxes']:
                for thing in bp.keys():
                    for line in bp[thing]:
                        line.set_color(color)
                del stacked, color, df, summary
            ax.legend()
            pdf_collection.savefig(fig)
    else:
        dfs = []
        for folder in folder_list:
            for binary in versions: #Here we have a CSV per binary, and use folders as versions
                binary_name = binary.replace(prefix + '_', '', 1).replace('.exe', '', 1)
                binary_type, binary_version = binary_name.split('-', 1)
                binary_id = binary_version.split('-')[-1]
                binary_build = ' '.join(binary_version.split('-')[:-1])

                folder_path = os.path.join(root, folder)
                if not os.path.isdir(folder_path):
                    raise Exception('Could not find folder {!s}, abort.'.format(folder_path))
                file = os.path.join(folder_path, binary + '.csv')
                if not os.path.isfile(file):
                    print('Could not find binary {!s} in folder {!s}, skip?'.format(binary, folder_path))
                    #raise Exception('abort')
                    continue

                df = pd.read_csv(file, index_col=[0, 1, 2])
                df.columns = [binary_id]
                df_stacked, summary = process_frame(df, prefix)
                df_stacked.columns = pd.MultiIndex.from_product([df_stacked.columns, [folder]])
                dfs.append(df_stacked)
                del df
        stacked = pd.concat(dfs, axis=1)
        stacked = stacked.loc[:, stacked.columns.sort_values()]

        fig, ax = plt.subplots()
        ax.set_xlabel('binary id')
        ax.set_ylabel('runtime per trial [s]')
        ax.set_title(binary_build)
        colors_base = ['#e41a1c','#377eb8','#4daf4a','#984ea3','#ff7f00','#ffff33','#a65628','#f781bf','#999999']
        from itertools import repeat
        colors = {folder: cl for folder, cl in zip(folder_list, colors_base)}

        max_xlim = 0
        stacked = stacked.stack(0).unstack(1) # Fill missing runs with NaNs
        stacked.columns = stacked.columns.swaplevel(0, 1) # And make levels the same
        for folder in folder_list:
            df = stacked.loc[:, (slice(None), folder)]
            df.columns = df.columns.droplevel(1)

            summ = df.mean().to_frame()
            line, = ax.plot(np.arange(len(summ)) + 1, summ, alpha=0.5, label=folder)
            color = line.get_color()
            bp = df.plot.box(ax=ax,
                             flierprops={'markeredgecolor': color},
                             return_type='dict',
                             #positions=stacked.columns.get_level_values(0).values,
                             #widths=min(np.diff(stacked.columns)),
                             )
            max_xlim = max(max_xlim, ax.get_xlim()[1])
        ax.set_xlim(None, max_xlim)
        ax.legend()
        pdf_collection.savefig(fig)
    pdf_collection.close()

if __name__ == '__main__':
    default_folder_list = [
        #'profile_master_7555c64',
        #'profile_jetexp_2ee8dd7',
        'profile_jetexp_2d27bbf',
    ]
    parser = argparse.ArgumentParser(description='Plot tools for binary profiling.')
    parser.add_argument('prefix', type=str, help='')
    parser.add_argument('folderlist', type=str, help='', nargs='*')
    args = parser.parse_args()
    if not args.folderlist:
        folderlist = default_folder_list
    else:
        folderlist = args.folderlist

    print(folderlist)
    generate_report(folderlist, root='.', output_path='report.pdf', prefix=args.prefix)
