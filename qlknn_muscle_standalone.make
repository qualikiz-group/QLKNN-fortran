# This file is part of QLKNN-fortran
# You should have received the QLKNN-fortran LICENSE in the root of the project
$(call SUBPROJECT_depend, QLKNN CLAF90)

$(call SUBPROJECT_reset_local)
$(call SUBPROJECT_set_local_dir_here)

# Find Muscle libraries
MUSCLE3_SUFF=.local/muscle3
ifeq ($(LOCALCFG_ACTIVE),docker-debian)
  MUSCLE3_PREFIX?=/builds/qualikiz-group/QLKNN-fortran/$(MUSCLE3_SUFF)
endif
MUSCLE3_INSTALL_DIR?=/builds/qualikiz-group/QLKNN-fortran/$(MUSCLE3_SUFF)

ifeq ($(TOOLCHAIN),gcc)
  $(info Detected TOOLCHAIN=$(TOOLCHAIN))
  QLKNN_EXTRA_FFLAGS+=-I${MUSCLE3_INSTALL_DIR}/include
  QLKNN_EXTRA_LFLAGS+=-L${MUSCLE3_INSTALL_DIR}/lib -lymmsl_fortran -lmuscle_mpi_fortran -lymmsl -lmuscle_mpi -lmsgpackc
else ifeq ($(TOOLCHAIN),intel)
  $(info Detected TOOLCHAIN=$(TOOLCHAIN))
  $(error Intel unsupported for QLKNN-MUSCLE)
else ifeq ($(TOOLCHAIN),pgi)
  $(info Detected TOOLCHAIN=$(TOOLCHAIN))
  $(error PGI unsupported for QLKNN-MUSCLE)
else
  $(error Unknown TOOLCHAIN=$(TOOLCHAIN))
endif

ifeq ($(TUBSCFG_MPI),1)
  $(info Detected MPI compilation, TUBSCFG_MPI=$(TUBSCFG_MPI))
else ifeq ($(TUBSCFG_MPI),0)
  $(info Serial compilation, TUBSCFG_MPI=$(TUBSCFG_MPI))
endif

# Check if we are compiling with MKL
ifeq ($(TUBSCFG_MKL),1)
  $(info Detected MKL compilation, TUBSCFG_MKL=$(TUBSCFG_MKL))
else ifeq ($(TUBSCFG_MKL),0)
  $(info Not compiling with MKL, TUBSCFG_MKL=$(TUBSCFG_MKL))
else
  $(info Unknown TUBSCFG_MKL $(TUBSCFG_MKL))
endif

$(eval include $(QLKNNROOT_)/flags.make)

$(call LOCAL_add,\
  src/qlknn_muscle_standalone.f90 \
)

$(call LOCAL_mod_dep, src/qlknn_muscle_standalone.f90, kinds.mod )
