# Shamelessly stolen from https://github.com/fortran-lang/stdlib/blob/master/config/DefaultFlags.cmake
# 0f88076854a4583758d195155a7ba82002a7ee3f
# Add new build types
# message("* Adding build types...")
# SET(CMAKE_CXX_FLAGS_COVERAGE
#     "${GCC_DEBUG_FLAGS} -fprofile-arcs -ftest-coverage"
#     CACHE STRING "Flags used by the C++ compiler during coverage builds."
#     FORCE )
# SET(CMAKE_C_FLAGS_COVERAGE
#     "${GCC_DEBUG_FLAGS} -fprofile-arcs -ftest-coverage"
#     CACHE STRING "Flags used by the C compiler during coverage builds."
#     FORCE )
# SET(CMAKE_EXE_LINKER_FLAGS_COVERAGE
#     ""
#     CACHE STRING "Flags used for linking binaries during coverage builds."
#     FORCE )
# SET(CMAKE_SHARED_LINKER_FLAGS_COVERAGE
#     ""
#     CACHE STRING "Flags used by the shared libraries linker during coverage builds."
#     FORCE )
# MARK_AS_ADVANCED(
#     CMAKE_CXX_FLAGS_COVERAGE
#     CMAKE_C_FLAGS_COVERAGE
#     CMAKE_EXE_LINKER_FLAGS_COVERAGE
#     CMAKE_SHARED_LINKER_FLAGS_COVERAGE
# )

message("** Settings default flags for QLKNN_BUILD_MPI=${QLKNN_BUILD_MPI}, FIRST_RUN=${FIRST_RUN}")
message("** Compiler ${CMAKE_Fortran_COMPILER_ID}")
if(CMAKE_Fortran_COMPILER_ID STREQUAL "GNU")
  set(
    CMAKE_Fortran_FLAGS_INIT
    "-fimplicit-none"
    "-Wall"
    "-fPIC"
    "-fopenmp"
    "-flto"
    "-ffree-line-length-256"
    "-Werror"
    "-Wall"
    "-cpp"
    "-std=f2008"
    "-O3"
    "-Wno-error"
    "-fno-lto"
    "-Wno-error"
  )
  if(QLKNN_BUILD_MPI)
    set(
        CMAKE_Fortran_FLAGS_INIT
        "${CMAKE_Fortran_FLAGS_INIT} -DMPI -I/usr/lib -cpp"
    )
  endif()
  set(
    CMAKE_Fortran_FLAGS_RELEASE_INIT
    "-O2"
    "-march=native"
    "-cpp"
  )
  set(
    CMAKE_Fortran_FLAGS_DEBUG_INIT
    "-Wall"
    "-Wextra"
    "-Wimplicit-procedure"
    "-std=f2018"
    "-O0"
    "-g3"
    "-cpp"
  )
elseif(CMAKE_Fortran_COMPILER_ID MATCHES "^Intel")
  set(
    CMAKE_Fortran_FLAGS_INIT
  )
  set(
    CMAKE_Fortran_FLAGS_RELEASE_INIT
  )
  if(WIN32)
    set(
      CMAKE_Fortran_FLAGS_DEBUG_INIT
      "/stand:f18"
      "/warn:declarations,general,usage,interfaces,unused"
    )
  else()
    set(
      CMAKE_Fortran_FLAGS_DEBUG_INIT
      "-stand f18"
      "-warn declarations,general,usage,interfaces,unused"
    )
  endif()
else()
  set(
    CMAKE_Fortran_FLAGS_INIT
  )
  set(
    CMAKE_Fortran_FLAGS_RELEASE_INIT
  )
  set(
    CMAKE_Fortran_FLAGS_DEBUG_INIT
  )
endif()
string(REPLACE ";" " " CMAKE_Fortran_FLAGS_INIT "${CMAKE_Fortran_FLAGS_INIT}")
string(REPLACE ";" " " CMAKE_Fortran_FLAGS_RELEASE_INIT "${CMAKE_Fortran_FLAGS_RELEASE_INIT}")
string(REPLACE ";" " " CMAKE_Fortran_FLAGS_DEBUG_INIT "${CMAKE_Fortran_FLAGS_DEBUG_INIT}")
