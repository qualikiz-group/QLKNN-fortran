program qlknn_muscle
#include "core/preprocessor.inc"
  use qlknn_types
  use qlknn_evaluate_nets
  use qlknn_version
  use qlknn_disk_io
  use cla
  use kinds
#ifdef MPI
  use MPI_WRAPPER
  use libmuscle_mpi
#else
  use libmuscle
#endif
  use ymmsl
  implicit none

  ! Muscle data
  type(LIBMUSCLE_PortsDescription) :: ports
  type(LIBMUSCLE_Instance) :: instance
  type(LIBMUSCLE_Message) :: rmsg
  type(LIBMUSCLE_DataConstRef) :: rdata
  type(LIBMUSCLE_Message) :: smsg
  type(LIBMUSCLE_Data) :: sdata
  real (selected_real_kind(15)) :: t_cur, t_next
  real (selected_real_kind(15)), dimension(:,:), allocatable :: U
  integer (LIBMUSCLE_size), dimension(2) :: shp
  integer (LIBMUSCLE_size) :: my_num_dims
  integer (LIBMUSCLE_size) :: siz

  ! QLKNN data
  integer(lli) :: n_trials, trial, verbosity, n_rho, n_in, n_out, n_outputs, n_nets, n_total_inputs
  type (qlknn_options) :: opts
  type (qlknn_normpars) :: qlknn_norms
  real(qlknn_dp), dimension(:,:), allocatable :: input
  real(qlknn_dp) :: start, finish
  real(qlknn_dp), dimension(:,:), allocatable :: qlknn_out
  real(qlknn_dp), dimension(:,:,:), allocatable :: dqlknn_out_dinput
  character(len=4096) :: nn_path = '', input_path = ''
  logical :: calculate_jacobian, dump_to_disk

  ! MPI data
#if defined(LLI) && defined(__INTEL_COMPILER)
    integer(lli) my_world_rank
#else
    integer(li) my_world_rank
#endif
#ifdef MPI
#if defined(LLI) && defined(__INTEL_COMPILER)
    integer(lli) mpi_ierr, world_size
#else
    integer(li) mpi_ierr, world_size
#endif
#endif
  integer, parameter :: root_rank = 0

  namelist /sizes/ n_rho, n_in
  namelist /test/ input
  namelist /outp_sizes/ n_rho, n_in, n_out
  namelist /outp/ qlknn_out
  namelist /outp_jacobian/ dqlknn_out_dinput

    ! Set up MPI et al.
#ifdef MPI
    call MPI_INIT(mpi_ierr)
    call MPI_COMM_SIZE(MPI_COMM_WORLD, world_size, mpi_ierr)
    call MPI_COMM_RANK(MPI_COMM_WORLD, my_world_rank, mpi_ierr)
#else
    my_world_rank = 0
#endif
  ! Print MPI debugging
  if (verbosity >= 1) then
    if (my_world_rank == root_rank) then
      print *, 'World size is         ', world_size
    end if
    print *, 'Hello world from rank ', my_world_rank
  endif


  ! Prepare Muscle
  ports = LIBMUSCLE_PortsDescription_create()
  call ports%add(YMMSL_Operator_F_INIT, 'initial_state')
  call ports%add(YMMSL_Operator_O_F, 'final_state')
  instance = LIBMUSCLE_Instance(ports)
  call LIBMUSCLE_PortsDescription_free(ports)

  ! Read Muscle options
  nn_path = instance%get_setting_as_character('nn_path')
  !call instance%get_setting_as_real8array2('in_init', input)
  input_path = instance%get_setting_as_character('input')
  !output_path = instance%get_setting_as_character('output')
  calculate_jacobian = instance%get_setting_as_logical('jacobian')
  dump_to_disk = instance%get_setting_as_logical('dump_to_disk')
  n_trials = instance%get_setting_as_int8('trials')
  verbosity = instance%get_setting_as_int8('verbosity')

  ! Evaluate QLKNN
  do while (instance%reuse_instance())
    rmsg = instance%receive('initial_state') ! LIBMUSCLE_Message
    write(stdout,*) my_world_rank, " received the initial state"
    ! Process Muscle data
    if (my_world_rank == root_rank) then
      rdata = rmsg%get_data() ! LIBMUSCLE_DataConstRef
      !call rdata%elements(U_all)
      !call LIBMUSCLE_DataConstRef_free(rdata)
      write(stdout,*) "Rank ", my_world_rank, " is processing data"
      if (rdata%is_a_list()) print *, 'rdata is a list!'
      if (rdata%is_a_dict()) print *, 'rdata is a dict!'
      if (rdata%is_a_byte_array()) print *, 'rdata is a byte array!'
      if (rdata%is_a_grid_of_real8()) print *, 'rdata is a grid of real8!'
      if (rdata%is_a_real8()) print *, 'rdata is a real8!'
      siz = rdata%size()
      print *, 'received size is', siz ! eg 264
      call rdata%shape(shp)
      print *, 'received shape is', shp ! eg [24, 11, memory garble]
      my_num_dims = rdata%num_dims()
      print *, 'received num_dims is', my_num_dims ! eg 2
      if (my_num_dims == 2_LIBMUSCLE_size) then
        allocate(U(shp(1), shp(2)))
        call rdata%elements(U)
      end if
      if (my_world_rank == root_rank .and. verbosity >= 2) THEN
        ! YOLO quartering
        !print *, 'received U is', U(:,1) ! Rho=1
        print *, 'received U with shape', shp(1:2), 'on', \
          my_world_rank, 'is:' // achar(13) // achar(10), \
          U(2,:), \
          achar(13) // achar(10) // ".." // achar(13) // achar(10), \
          U(shp(1),:)
      endif
    endif ! my_world_rank==root_rank

    t_cur = rmsg%timestamp()
    t_next = rmsg%next_timestamp()
    !t_end = rmsg%timestamp() + t_max
    call LIBMUSCLE_Message_free(rmsg)

    call MPI_Bcast(siz, 1, MPI_LONG, root_rank, MPI_COMM_WORLD, mpi_ierr)
    call MPI_Bcast(shp, 2, MPI_LONG, root_rank, MPI_COMM_WORLD, mpi_ierr)
    call MPI_Bcast(t_cur, 1, MPI_DOUBLE, root_rank, MPI_COMM_WORLD, mpi_ierr)
    call MPI_Bcast(t_next, 1, MPI_DOUBLE, root_rank, MPI_COMM_WORLD, mpi_ierr)
    write(stdout,*) my_world_rank, " siz=", siz, "shp=", shp, "t_cur=", t_cur, "t_next=", t_next
    if (my_world_rank == root_rank) then
      allocate(input(shp(2), shp(1)))
      input = transpose(U)
      deallocate(U)
    else
      allocate(input(shp(2), shp(1)))
    end if
    call MPI_Bcast(input, int(siz, li), MPI_DOUBLE, root_rank, MPI_COMM_WORLD, mpi_ierr)
    write(stdout,*) my_world_rank, " is done with processing state"

    ! Prepare QLKNN-fortran options
    call default_qlknn_hyper_options(opts)

    opts%merge_modes = instance%get_setting_as_logical('merge_modes')
    opts%apply_victor_rule = instance%get_setting_as_logical('victor_rule')
    opts%force_evaluate_all = instance%get_setting_as_logical('force_evaluate_all')
    opts%use_effective_diffusivity = instance%get_setting_as_logical('use_effective_diffusivity')

    !! Read input namelist
    !write(stdout,*) "Reading '", TRIM(input_path), "'"
    !open(10,file=input_path,action='READ')
    !read(10,nml=sizes)
    !allocate(input(n_in, n_rho))
    !read(10,nml=test)
    !close(10)
    n_total_inputs = int(size(input, 1), li)

    n_outputs = 10
    n_nets = 20
    if (opts%merge_modes) then
      n_out = n_outputs
    else
      n_out = n_nets
    end if
    n_rho = int(size(input, 2), li)
    allocate(qlknn_out(n_rho, n_out))
    allocate(dqlknn_out_dinput(n_out, n_rho, n_total_inputs))

    ! Variables for Victor rule
    ALLOCATE(qlknn_norms%A1(n_rho))
    qlknn_norms%A1 = 2.
    qlknn_norms%R0 = 3.
    qlknn_norms%a = 1.

    ! Print runtime debugging
    if (my_world_rank==root_rank) then
      write(stdout,*) ' _________________________________________________________________________________ '
      write(stdout,*) '                                 QLKNN-fortran ' // QLKNN_CLOSEST_RELEASE
      write(stdout,*) '                       Machine learning surrogate of QuaLiKiz'
      write(stdout,*) ' _________________________________________________________________________________ '
      write(stdout,*) ' '
      if (verbosity >= 1) THEN
        write(stdout, *) 'QLKNN-fortran REPOSITORY STATUS DURING COMPILATION'
        write(stdout, *) '---------------------------------------------'
        write(stdout, *) 'From GIT repository:     ' // QLKNN_REPOSITORY_ROOT
        write(stdout, *) 'On GIT branch:           ' // QLKNN_GITBRANCHNAME
        write(stdout, *) 'Last commit SHA1-key:    ' // QLKNN_GITSHAKEY
        write(stdout, *) 'Hostname at compilation: ' // QLKNN_HOSTFULL
        write(stdout, *) 'Compiler version:        ' // QLKNN_COMPILER_VERSION
        write(stdout,*) ' '
      endif
      write(stdout,'(A,I10)') ' Amount of parallel processors = ', world_size
      write(stdout,'(A,I10)') ' Number of radial or scan points (dimx)  = ', size(input, 2)
      write(stdout,'(A,I10)') ' Number of input variables =', size(input, 1)
      write(stdout,*) ' '
    endif


    call load_qlknn_hyper_nets_from_disk(nn_path, verbosity)


    call MPI_Barrier(MPI_COMM_WORLD)
    write(stdout,*) 'Rank', my_world_rank, 'starting embarrassingly parallel part'
    call cpu_time(start)
    do trial = 1,n_trials
      if (.not. calculate_jacobian) then
        call evaluate_QLKNN_10D(input, nets, qlknn_out, verbosity, opts, qlknn_norms)
      else
        call evaluate_QLKNN_10D(input, nets, qlknn_out, verbosity, opts, qlknn_norms, dqlknn_out_dinput)
      endif
    end do

    call MPI_Barrier(MPI_COMM_WORLD)
    write(stdout,*) 'Rank', my_world_rank, 'done with the embarrassingly parallel part'

    ! Only Fans
    if (my_world_rank == root_rank) then
      write(stdout,*) "Rank ", my_world_rank, " sending message to our overlord"
      !sdata = LIBMUSCLE_Data_create_grid_2_real8_a(qlknn_out)
      sdata = LIBMUSCLE_Data_create_grid(qlknn_out)
      smsg = LIBMUSCLE_Message(t_cur, sdata)
      call instance%send('final_state', smsg)

      call LIBMUSCLE_Message_free(smsg)
      call LIBMUSCLE_Data_free(sdata)
    endif

    deallocate(input, qlknn_out, dqlknn_out_dinput, qlknn_norms%A1)
    call all_networktype_deallocate(nets)
    call cpu_time(finish)
  end do

  write(stdout,*) my_world_rank, " is waiting till all are done"
  if (my_world_rank == root_rank) then
    print '("Trials = ",i9)',n_trials
    print '("Time   = ",f9.3," milliseconds.")',1e3*(finish-start)/n_trials
  endif


  call LIBMUSCLE_Instance_free(instance)
#ifdef MPI
  call MPI_FINALIZE(mpi_ierr) ! Call after LIBMUSCLE_Instance_free
#endif

end program qlknn_muscle
