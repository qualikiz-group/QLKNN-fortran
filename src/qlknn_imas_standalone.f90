program qlknn_imas_standalone

  use ids_schemas, only: ids_equilibrium
  use ids_routines, only: imas_open_env,imas_create_env,imas_close,ids_get,ids_put

  implicit none

  integer:: idx
  type(ids_equilibrium):: equilibrium_in,equilibrium_out
  character(len=200):: user,machine

  ! DEFINE LOCAL DATABASE
  !call getenv('USER',user)
  machine = 'iter'

  ! OPEN INPUT DATAFILE FROM OFFICIAL IMAS SCENARIO DATABASE
  write(*,*) '=> Read input IDSs'
  call imas_open_env('ids',131024,0,idx,'public','iter','3')
  call ids_get(idx,'equilibrium',equilibrium_in)
  call imas_close(idx)
  write(*,*) 'Finished reading input IDSs'

  ! EXECUTE PHYSICS CODE
  call physics_i(equilibrium_in,equilibrium_out)

  ! EXPORT RESULTS TO LOCAL DATABASE
  write(*,*) '=> Export output IDSs to local database'
  call imas_create_env('ids',131024,2,0,0,idx,trim(user),trim(machine),'3')
  call ids_put(idx,'equilibrium',equilibrium_out)
  call imas_close(idx)
  write(*,*) 'Done exporting.'
  write(*,*) ' '
  write(*,*) 'End of wrapper'

end program qlknn_imas_standalone
