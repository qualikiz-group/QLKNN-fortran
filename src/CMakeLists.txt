# SPDX-Identifier: MIT
set(dir "${CMAKE_CURRENT_SOURCE_DIR}")

set(
  srcs
  "${dir}/qlknn_hyper_standalone.f90"
)

set(CMAKE_USER_MAKE_RULES_OVERRIDE ${CMAKE_CURRENT_SOURCE_DIR}/../config/QLKNNFortranDefaultFlags.cmake)

set(
  MODULE_OUTPUT_DIR
  "${CMAKE_CURRENT_BINARY_DIR}/include"
)
set(CMAKE_Fortran_MODULE_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}/modules)
SET(CMAKE_MODULE_PATH ${CMAKE_CURRENT_BINARY_DIR}/modules)

add_executable(simple "${dir}/qlknn_hyper_standalone.f90")
target_link_libraries(simple ${CORE_NAME}-lib "${CLAF90_NAME}-lib")

set(CMAKE_REQUIRED_FLAGS -cpp)
include(CheckSourceCompiles)
include(CheckSourceRuns)

# Untested compiler flags
#if !__has_c_attribute(noreturn)
#if defined(LLI) && defined(__INTEL_COMPILER)
#if MX_HAS_INTERLEAVED_COMPLEX
#error \"No noreturn attribute\"
#if (defined(LLI) && defined(__INTEL_COMPILER)) || defined(MEXING)
set(CMAKE_TRY_COMPILE_TARGET_TYPE "STATIC_LIBRARY")
#use qlknn_evaluate_nets
check_source_runs(Fortran
"program test
#ifdef MPI
write(*,*) 'Hello World! This is a very long line that basically tests the trucation values of our compiler. It should end at some point..'
#endif
close(0)
end program"
    HAVE_FORTRAN
)
message("HAVE_FORTRAN=${HAVE_FORTRAN}")
