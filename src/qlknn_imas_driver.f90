subroutine physics_i(equilibrium_in,equilibrium_out)

  ! ---------------------------------------
  ! PURPOSE: SIMPLE PSEUDO PHYSICS CODE 
  ! (IT READS AN EQUILIBRIUM IDS,
  ! MODIFIES ONE VARIABLE IN THE IDS, 
  ! AND WRITES THE NEW EQUILIBRIUM IDS)
  ! ---------------------------------------

  use ids_schemas, only: ids_equilibrium
  use ids_routines, only: ids_copy

  implicit none

  type(ids_equilibrium):: equilibrium_in,equilibrium_out

  ! INITIAL DISPLAY
  write(*,*) ' '
  write(*,*) '======================================='
  write(*,*) 'START OF PHYSICS CODE'

  ! COPY THE INPUT IDS IN THE OUTPUT IDS
  call ids_copy(equilibrium_in,equilibrium_out)

  ! INITIAL PLASMA MAJOR RADIUS
  write(*,'(a31,f7.3)') ' Initial plasma major radius = ', &
       equilibrium_in%time_slice(1)%boundary%geometric_axis%r

  ! MODIFY PLASMA MAJOR RADIUS
  equilibrium_out%time_slice(1)%boundary%geometric_axis%r = &
       equilibrium_out%time_slice(1)%boundary%geometric_axis%r * 1.5

  ! MANDATORY FLAG (UNIFORM TIME HERE)
  equilibrium_out%ids_properties%homogeneous_time = 1

  ! FINAL PLASMA MAJOR RADIUS
  write(*,'(a31,f7.3)') ' Final plasma major radius   = ', &
       equilibrium_out%time_slice(1)%boundary%geometric_axis%r

  ! FINAL DISPLAY
  write(*,*) 'END OF PHYSICS CODE'
  write(*,*) '======================================='
  write(*,*) ' '

end subroutine physics_i
