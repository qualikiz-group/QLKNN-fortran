src_dir: ./src
output_dir: ./doc
project: QLKNN-fortran
project_website: https://gitlab.com/qualikiz-group/QLKNN-fortran/wikis
project_download: https://gitlab.com/Karel-van-de-Plassche/QLKNN-develop
summary: FORTRAN interface for QuaLiKiz Neural Networks (QLKNN)
author: Karel van de Plassche
author_description: I am a junior researcher at the Dutch Institude For Fundamental Energy Research ([DIFFER](https://www.differ.nl/)). In our Integrated Modelling and Transport ([IMT](https://www.differ.nl/research/integrated-modelling-and-transport)) group we model and support models for full-device modelling of Tokamaks. I mostly focus on quick modelling of core transport using machine learning techniques, but we are always interested to learn more!)
github: https://gitlab.com/Karel-van-de-Plassche
email: k.l.vandeplassche@differ.nl
linkedin: https://www.linkedin.com/in/karelvandeplassche
fpp_extensions: f90
predocmark: >
media_dir: ./media
docmark_alt: #
predocmark_alt: <
display: public
         protected
         private
source: true
graph: true
coloured_edges: true
search: true
macro: MPI
warn: true
page_dir: .

Please refer to the [wiki](https://gitlab.com/qualikiz-group/QLKNN-fortran/wikis) for more usage-type documentation. This page contains detailed information about all methods and types defined in QLKNN-fortran.
